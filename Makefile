SHELL=/bin/bash
CC=gcc
CPP=g++
CCSTD=-std=gnu11
CXXSTD=-std=gnu++11
CFLAGS=-O3 -fdiagnostics-color=auto -fno-exceptions -pthread -g $(CCSTD)
CXXFLAGS=$(filter-out $(CCSTD), $(CFLAGS)) $(CXXSTD) -Wno-write-strings -Wno-pointer-arith
MKDIRS=lib bin tst/bin .pass .pass/tst/bin .make .make/bin .make/tst/bin .make/lib
INCLUDE=$(addprefix -I,include include/cds)
EXECS=$(addprefix bin/,)
TESTS=$(addprefix tst/bin/,concat spsc mpsc stream builtins utils fifo eo set dict fifo)
BENCHMARKS=$(addprefix tst/bin/bench, dict feld mich fifo singledict singlefeld)
JBENCHMARKS=$(addsuffix .class,$(addprefix tst/bench, kogan))
SRC=$(wildcard src/*.cpp)
LIBS=$(patsubst src/%.cpp, lib/%.o, $(SRC))


.PHONY: default all clean again check distcheck dist-check
default: all
all: $(EXECS) $(TESTS) $(BENCHMARKS) $(JBENCHMARKS)
again: clean all
check: $(addprefix .pass/,$(TESTS))
FNM=\([a-z_A-Z/0-9]*\)
.make/%.d: %.c
	@mkdir -p $(@D)
	@$(CC) -MM $(CCSTD) $(INCLUDE) $< -o $@
.make/%.d: %.cpp
	@mkdir -p $(@D)
	$(CPP) -MM $(CXXSTD) $(INCLUDE) $< -o $@
.make/lib/%.o.d: .make/src/%.d | .make/lib
	@sed 's/$(FNM)\.o/lib\/\1.o/g' $< > $@
.make/bin/%.d: .make/%.d | .make/bin
	@sed 's/$(FNM).o:/bin\/\1:/g' $< > $@
	@perl make/depend.pl $@ > $@.bak
	@mv $@.bak $@
.make/tst/bin/%.d: .make/tst/%.d | .make/tst/bin
	@sed 's/$(FNM).o:/tst\/bin\/\1:/g' $< > $@
	@perl make/depend.pl $@ > $@.bak
	@mv $@.bak $@
MAKES=$(addsuffix .d,$(addprefix .make/, $(EXECS) $(TESTS) $(BENCHMARKS) $(LIBS)))
-include $(MAKES)
.SECONDARY: $(LIBS) $(MAKES)
distcheck dist-check:
	@rm -rf .pass
	@make --no-print-directory check
.pass/tst/bin/%: tst/bin/% | .pass/tst/bin
	@printf "$<: "
	@$<\
		&& echo -e "\033[0;32mpass\033[0m" && touch $@\
		|| echo -e "\033[0;31mfail\033[0m"
.pass/tst/bin/builtins: tst/bin/builtins | .pass/tst/bin
	@printf "$<: "
	@echo pass: stdin | $< arg1 | diff - <(echo pass: stdout;echo pass: stdin)\
		   && echo -e "\033[0;32mpass\033[0m" && touch $@\
		   || echo -e "\033[0;31mfail\033[0m"
$(MKDIRS):
	@mkdir -p $@
$(EXECS): | bin
bin/%: %.cpp
	$(CPP) $(CXXFLAGS) $(INCLUDE) $^ -o $@
bin/%: %.c
	$(CC) $(CFLAGS) $(INCLUDE) $^ -o $@
lib/%.o: src/%.cpp include/%.h | lib
	$(CPP) -c $(CXXFLAGS) $(INCLUDE) $< -o $@
lib/%.o: src/%.c include/%.h | lib
	$(CC) -c $(CFLAGS) $(INCLUDE) $< -o $@
tst/bin/%: tst/%.cpp | tst/bin
	$(CPP) $(CXXFLAGS) $(INCLUDE) $(filter-out include/%.h,$^) -o $@
tst/bin/%: tst/%.c | tst/bin
	$(CC) $(CFLAGS) $(INCLUDE) $^ -o $@


FAKE=-fexceptions
$(FAKE):
	touch ./$@
.INTERMEDIATE: $(FAKE)
clean:
	rm -rf $(MKDIRS) $(addprefix ./,$(FAKE))

# cds deps
tst/bin/benchmich: -fexceptions $(addprefix include/cds/obj/gcc-amd64-linux-64/release/, hzp_gc.o hrc_gc.o init.o urcu_gp.o urcu_sh.o ptb_gc.o topology_linux.o)

# java
JC=javac
%.class: %.java
	$(JC) $^

# latex stuff
PAPERS=$(subst .tex,.pdf,$(wildcard papers/*.tex))
tex: $(PAPERS)
%.pdf: %.tex $(wildcard papers/*.dat) $(wildcard papers/code)
	pdflatex -output-directory $(@D) $<
