Libscion
=======

A collection of concurrent data structures.

Files
---------------------------

- __concat.h__ : stream; Single-reader, single-writer queue. Supports asynchronous fulfillment.

```cpp
concat<int> q1, *q2 = new concat<int>;
q1.give(q2);
q1.put(2);
q1.close();
q2->put(1);
q2->close();
q1.get(); // 1
q1.peek(); // 1
q1.get(); // 2
q1.depleted(); // true
```

- __stream.h__ : stream; Single-writer FIFO queue. All readers receive all data.
Supports asynchronous fulfillment.

```cpp
stream<int> q1, *q2 = new stream<int>;
key<int> *kQ1 = q1.listen(),
         *kQ2 = q2->listen(),
         *kQ3 = q2->listen();
q1.give(kQ2);
ready(kQ2); // false
q1.put(2);
q1.close();
q2->put(1);
q2->close();
from(kQ3); // 1
cancel(kQ3);
from(kQ1); // 1
peek(kQ1); // 2
empty(kQ1); // false
from(kQ1); // 2
empty(kQ1); // true
depleted(kQ1); // true
```

- __mpsc.h__ : queue; Single-reader, multi-writer FIFO queue.
Wait-free.

```cpp
mpsc<int,2> q;
q.enq(2,1); // producer 1
q.enq(3,0); // producer 0
q.deq(); // 2
q.deq(); // 3
q.deq(); // DNF
```

- __fifo.h__  : queue; Multi-reader, multi-writer queue. Wait-free.
Implements Alex Kogan's Wait Free Queue using Maged Michael's hazard pointers instead of garbage collection.
Supports enq and deq.
All enqueued items are dequeued exactly once.

```cpp
fifo<int> q;
auto kQ = q.subscribe();
q.enq(kQ, 1);
q.deq(kQ); // 1
```

- __set.h__ : set; Multi-reader, multi-writer set with add and remove.
Writers knows how they modified the set.

- __dict.h__  : dictionary; Multi-reader, multi-writer dictionary.
Wait-free for most key types.
Supports put, get, remove, fetch\_add, cas, and find.
Future support for iteration planned.

```cpp
dict<int, int> d;
d.put(1,2);
d.get(1); // 2
d.remove(1);
d.get(1); // DNF
```

Notes:
------

- Must compile and link with -pthread
