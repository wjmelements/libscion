package tst;
import include.kogan.OptWFQueue;
// note : need to adjust numThreads in OptWFQueue when changing number of threads
public class benchkogan implements Runnable {
    private static final OptWFQueue queue = new OptWFQueue();
    private static final int NUM_ITEMS = 800000;

    private int tid;
    public void run() {
        int threadId = this.tid;
        for (int i = 0 ; i < NUM_ITEMS; i++) {
            queue.enq(threadId, i);
            try {
                queue.deq(threadId);
            } catch (OptWFQueue.EmptyException e) {
                System.exit(-1);
            }
        }
    }
    private benchkogan(int tid) {
        this.tid = tid;
    }

    public static void main(String cheese[]) {
        long time = System.currentTimeMillis();
        Thread[] threads = new Thread[8];
        for (int i = 0 ; i < 8; i++) {
            threads[i] = new Thread(new benchkogan(i));
            threads[i].start();
        }
        for (int i = 0; i < 8; i++) {
            try {
                threads[i].join();
            } catch(InterruptedException e) {
                System.exit(-1);
            }
        }
        System.out.printf("Real: %dms\n", (System.currentTimeMillis() -  time));
    }
}
