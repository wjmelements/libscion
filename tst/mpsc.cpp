#include "mpsc.h"

#include <assert.h>
#include <pthread.h>
#include <stdio.h>

#include <map>
using std::map;
using std::pair;


void basic_test() {
    mpsc<int,1> q;
    assert(!q.deq());
    q.enq(1,0);
    assert(1 == *q.deq());
    assert(!q.deq());
    q.enq(2,0);
    q.enq(3,0);
    assert(2 == *q.deq());
    assert(3 == *q.deq());
    assert(!q.deq());
}

#define NTHREADS 16
struct para_arg {
    mpsc<int,NTHREADS>* q;
    int id;
};
#define NITEMS 300
void* para_thread(void* arg) {
    para_arg* parg = (para_arg*) arg;
    mpsc<int,NTHREADS>* q = parg->q;
    int id = parg->id;
    free(arg);
    for (int i = 1; i <= NITEMS; i++) {
        q->enq(i,id);
    }
    return NULL;
}
void para_test() {
    mpsc<int,NTHREADS> q;
    pthread_t threads[NTHREADS];
    for (size_t i = 0; i < NTHREADS; i++) {
        struct para_arg* parg = (para_arg*) malloc(sizeof *parg);
        parg->q = &q;
        parg->id = i;
        pthread_create(&threads[i], NULL, para_thread, parg);
    }
    map<int, size_t> counts;
    while (1) {
        optional<int> got = q.deq();
        if (got) {
            int val = *got;
            counts[val]++;
            if (val == NITEMS && counts[val] == NTHREADS) {
                break;
            }
        } else {
            sched_yield();
        }
    }
    for (size_t i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    for (auto pair : counts) {
        assert(pair.second == NTHREADS);
    }
    assert(counts.size() == NITEMS);
}
struct worker_arg {
    int id;
    mpsc<pair<int, int>,NTHREADS>* q;
};
void* seq_cons_test_worker(void* arg) {
    struct worker_arg* warg = (worker_arg*) arg;
    int id = warg->id;
    mpsc<pair<int, int>,NTHREADS>* q = warg->q;
    free(warg);
    for (int i = 0; i < NITEMS; i++) {
        q->enq(pair<int, int>(id, i),id);
    }
    return NULL;
}
void seq_cons_test() {
    mpsc<pair<int, int>,NTHREADS> q;
    map<int, int> counts;
    for (int i = 0; i < NTHREADS; i++) {
        struct worker_arg* warg = (struct worker_arg*) malloc(sizeof(worker_arg));
        warg->id = i;
        warg->q = &q;
        pthread_t thread;
        pthread_create(&thread, NULL, seq_cons_test_worker, warg);
        pthread_detach(thread);
        counts[i] = 0;
    }
    do {
        optional<pair<int, int> >got = q.deq();
        if (got) {
            assert(counts[got->first] == got->second);
            if (++counts[got->first] == NITEMS) {
                counts.erase(got->first);
            }
        } else {
            sched_yield();
        }
    } while (counts.size());
}
int main() {
    basic_test();
    #define NTRIALS 3000
    for (unsigned int i = 0; i < NTRIALS; i++) {
        printf("%04X / %04X\b\b\b\b\b\b\b\b\b\b\b", i, NTRIALS);
        if (i < NTRIALS / 2) {
            para_test();
        } else {
            seq_cons_test();
        }
    }       
    printf("              \b\b\b\b\b\b\b\b\b\b\b\b\b\b");
    // verify sequential consistency
    return 0;
}
