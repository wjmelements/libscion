#include "fifo.h"
typedef fifo<int> queue;

#define NUM_ITEMS 800000
void* worker(void* arg) {
    queue* q = (queue*) arg;
    auto acc = q->subscribe();
    for (int i = 0; i < NUM_ITEMS; i++) {
        q->enq(acc,i);
        q->deq(acc);
    }
    return NULL;
}
#define NUM_THREADS 8
int main() {
    pthread_t threads[NUM_THREADS];
    queue items;
    items.subscribe();
    for (int i = 0 ; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, worker, &items);
    }
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}
