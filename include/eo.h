#include <atomic>
using std::atomic;
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
// FIXME random uses a lock

#include "optional.h"

template <typename T, size_t THROUGHPUT = 4> class eo {
    protected:
    struct eo_node {
            struct eo_node* next;
            T data;
            atomic<char> dibs;
        } **out;
        atomic<eo_node*>* in;
    public:
        eo();
        ~eo();
        optional<T> get();
        void put(T data);
        void put(T data, size_t iid);
        void close(size_t iid);
};
template <typename T, size_t THROUGHPUT> eo<T,THROUGHPUT>::eo() {
    this->out = (eo_node** ) malloc(THROUGHPUT * sizeof(eo_node* ));
    this->in  = (atomic<eo_node*>*) malloc(THROUGHPUT * sizeof(eo_node*));
    for (size_t i = 0; i < THROUGHPUT; i++) {
        this->out[i] = (eo_node*) malloc(sizeof(eo_node));
        this->out[i]->dibs = EXIT_SUCCESS;
        this->out[i]->next = NULL;
        this->in[i] = this->out[i];
    }
}
template <typename T, size_t THROUGHPUT> eo<T,THROUGHPUT>::~eo() {
    for (size_t i = 0 ; i < THROUGHPUT; i++) {
        while (this->out[i]) {
            eo_node* last = this->out[i];
            this->out[i] = this->out[i]->next;
            free(last);
        }
    }
    free(this->in);
    free(this->out);
}
template <typename T,size_t THROUGHPUT> void eo<T,THROUGHPUT>::put(T data) {
    size_t iid = random() % THROUGHPUT;
    this->put(data, iid);
}
template <typename T, size_t THROUGHPUT> void eo<T, THROUGHPUT>::put(T data, size_t iid) {
    eo_node* input = (eo_node*) malloc(sizeof(eo_node));
    eo_node* put = input;
    input->data = data;
    input->next = NULL;
    input->dibs = EXIT_SUCCESS;
    eo_node* prev = this->in[iid].exchange(put);
    // if prev is NULL this implies that this iid was CLOSED
    // FIXME preemption here can block getters from getting newer things
    prev->next = input;
}
template <typename T, size_t THROUGHPUT> void eo<T, THROUGHPUT>::close(size_t iid) {
    this->in[iid] = NULL;
}
template <typename T, size_t THROUGHPUT> optional<T> eo<T,THROUGHPUT>::get() {
    size_t j = random() % THROUGHPUT;
    size_t end = j + THROUGHPUT;
    size_t closed_sources = 0;
    optional<T> ret;
    for (; j < end; j++) {
        size_t i = j % THROUGHPUT;
        if (this->in[i].load() == NULL) {
            closed_sources++;
        }
        struct eo_node* old = this->out[i];
        while (old->next) {
            struct eo_node* pop = old->next;
            char to_put = EXIT_FAILURE;
            char got = pop->dibs.exchange(to_put);
            if (got == EXIT_SUCCESS) {
                // FIXME this can make it O(n) in worst case
                this->out[i] = pop;
                // TODO free(old)
                ret.valid = true;
                ret.value = pop->data;
                return ret;
            } else {
                // maybe try the next one
                old = pop;
            }
        }
    }
    if (closed_sources == THROUGHPUT) {
        ret.valid = false;
        return ret;
    }
    sleep(0);
    // FIXME this tail recursion can stack overflow and segfault on lower optimization levels
    return this->get();
}
