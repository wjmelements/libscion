#include <assert.h>


#include "utils.h"

#ifndef REPS
#define REPS 1000
#endif

// tests higher order function map
float* half(int* arg) {
    float* ret = (float*) malloc(sizeof(float));
    *ret = ((float) (*arg)) / 2;
    return ret;
}
void test_map_stream_serial(const int max) {
    for (int size = 0; size < max; size++) {
        stream<int*> wholes(2);
        key<int*>* to_free = wholes.listen();
        key<int*>* to_eat = wholes.listen();
        key<float*>** halfs = map_stream<int*,float*>(&to_eat, half, 1, 2);
        for (int i = 0; i < size; i++) {
            int* put = (int*)malloc(sizeof(int));
            *put = i;
            wholes.put(put);
        }
        wholes.close();
        for (int i = 0; i < size; i++) {
            float* out = from(halfs[0]);
            assert((float) i / 2 == *out);
            free(from(halfs[1]));
            free(from(to_free));
        }
        assert(depleted(to_free));
        assert(depleted(to_eat));
        for (size_t i = 0; i < 2; i++) {
            assert(depleted(halfs[i]));
            free(halfs[i]);
        }
        free(halfs);
    }
}
void test_map_stream_weave(const int max) {
    for (int size = 0; size < max; size++) {
        stream<int*> wholes(2);
        key<int*>* to_free = wholes.listen();
        key<int*>* to_eat = wholes.listen();
        key<float*>** halfs = map_stream<int*,float*>(&to_eat, half, 1, 2);
        for (int i = 0; i < size; i++) {
            int* put = (int*)malloc(sizeof(int));
            *put = i;
            wholes.put(put);
            float* out = from(halfs[0]);
            assert((float) i / 2 == *out);
            free(from(halfs[1]));
            free(from(to_free));
        }
        wholes.close();
        assert(depleted(to_free));
        assert(depleted(to_eat));
        for (int i = 0; i < 2; i++) {
            assert(depleted(halfs[i]));
            free(halfs[i]);
        }
        free(halfs);
    }
}
void* test_map_stream_counter(void* arg) {
    key<float*>* param = (key<float*>*) arg;
    size_t i = 0;
    size_t j = 0;
    while (!depleted(param)) {
        float* out = from(param);
        assert((float) i / 2 == *out || (float) j / 2 == *out);
        if ((float) i / 2 == *out) {
            i++;
        } else {
            j++;
        }
    }
    assert(i == j);
    return EXIT_SUCCESS;
}
void test_map_stream_para(const int max) {
    const int repstodo = (REPS / 2) / max + 1;
    for (int rep = 0; rep < repstodo; rep++) {
        for (int size = 0; size < max; size++) {
            printf("%04X / %04X", rep, repstodo);
            for (size_t i = 0; i < 11; i++) {
                putchar('\b');
            }
            key<int*>* consumers[2];
            stream<int*> wholes(3);
            consumers[0] = wholes.listen();
            consumers[1] = wholes.listen();
            key<int*>* to_free = wholes.listen();

            key<float*>** halfs = map_stream<int*,float*>(consumers, half, 2, 2);

            pthread_t receiver;
            pthread_create(&receiver, NULL, test_map_stream_counter, halfs[0]);

            for (int i = 0; i < size; i++) {
                int* put = (int*)malloc(sizeof(int));
                *put = i;
                wholes.put(put);
            }
            wholes.close();

            pthread_join(receiver, NULL);
            for (int i = 0; i < size; i++) {
                free(from(to_free));
            }
            for (int i = 0; i < size * 2; i++) {
                free(from(halfs[1]));
            }
            assert(depleted(to_free));
            free(to_free);
            for (size_t i = 0; i < 2; i++) {
                assert(depleted(halfs[i]));
                free(halfs[i]);
                assert(depleted(consumers[i]));
                free(consumers[i]);
            }
            free(halfs);
        }
    }
    for (size_t i = 0; i < 11; i++) {
        putchar(' ');
    }
    for (size_t i = 0; i < 11; i++) {
        putchar('\b');
    }
}

void test_map() {
    const int max = 120;
    for (int size = 0; size < max; size++) {
        int** wholes = (int**) malloc(sizeof(int**) * size);
        for (int i = 0; i < size; i++) {
            wholes[i] = (int*)malloc(sizeof(int));
            *wholes[i] = i;
        }
        float** halfs = map<int,float>(wholes, half, size);
        for (int i = 0; i < size; i++) {
            assert((float) i / 2 == *halfs[i]);
            free(halfs[i]);
            free(wholes[i]);
        }
        free(wholes);
        free(halfs);
    }
}
void test_map_stream() {
    const int max = 120;
    test_map_stream_serial(max);
    test_map_stream_weave(max);
    test_map_stream_para(max * 2);
}
int main() {
    srandom(time(NULL));
    test_map();
    test_map_stream();
}
