#include "fifo.h"
#include "spsc.h"

#include <assert.h>
#include <functional>
using std::pair;
#include <map>
using std::map;
#include <pthread.h>
#include <set>
using std::set;
#include <stdio.h>

void basic_test() {
    fifo<int> f;
    auto m = f.subscribe();
    assert(!f.deq(m));
    f.enq(m,1);
    f.enq(m,3);
    f.enq(m,2);
    assert(*f.deq(m) == 1);
    assert(*f.deq(m) == 3);
    assert(*f.deq(m) == 2);
    assert(!f.deq(m));
    f.enq(m,4);
    assert(*f.deq(m) == 4);
}
#ifndef NUM_THREADS
#define NUM_THREADS 16
#endif
#ifndef NITEMS
#define NITEMS 10000
#endif
void* fifo_producer(void* varg) {
    fifo<long> *f = (fifo<long>*) varg;
    auto m = f->subscribe();
    for (long i = 0; i < NITEMS; i++) {
        f->enq(m,i);
    }
    return NULL;
}
void paraput_test() {
    fifo<long> f;
    auto m = f.subscribe();
    assert(!f.deq(m));
    pthread_t threads[NUM_THREADS];
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, fifo_producer, &f);
    }
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
}
void* fifo_thread(void* varg) {
    fifo<long> *f = (fifo<long>*) varg;
    auto m = f->subscribe();
    for (long i = 0; i < NITEMS; i++) {
        f->enq(m,i);
    }
    for (long i = 0; i < NITEMS; i++) {
        optional<long> found = f->deq(m);
        assert(found);
        assert(*found >= 0 && *found < NITEMS);
    }
    return f;
}
void para_test() {
    fifo<long> f;
    auto m = f.subscribe();
    assert(!f.deq(m));
    pthread_t threads[NUM_THREADS];
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, fifo_thread, &f);
    }
    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    optional<long> got;
    bool extras = false;
    while ((got = f.deq(m)).valid) {
        printf("Additional value got: %li\n", got.value);
        extras = true;
    }
    assert(!extras);
}
struct seq_con_arg {
    fifo<pair<pthread_t,int> > *f;
    spsc<pair<pthread_t,int> > *q;
};
void* seq_con_job(void* varg) {
    seq_con_arg* arg = (seq_con_arg*) varg;
    fifo<pair<pthread_t,int> > *f = arg->f;
    spsc<pair<pthread_t,int> > *q = arg->q;
    pthread_t me = pthread_self();
    free(varg);

    auto m = f->subscribe();

    for (int i = 1; i <= NITEMS; i++) {
        auto got = f->deq(m);
        if (got.valid) {
            int val = got.value.second;
            assert(val > 0 && val <= NITEMS);
            q->enq(got.value);
        }
        pair<pthread_t,int> put(me, i);
        f->enq(m,put);
        got = f->deq(m);
        if (got.valid) {
            int val = got.value.second;
            assert(val > 0 && val <= NITEMS);
            q->enq(got.value);
        }
    }
    return NULL;
}
void seq_con_test() {
    fifo<pair<pthread_t,int> >f;
    auto m = f.subscribe();
    assert(!f.deq(m));
    pthread_t threads[NUM_THREADS];
    spsc<pair<pthread_t,int> > qs[NUM_THREADS];
    for (int i = 0; i < NUM_THREADS; i++) {
        seq_con_arg *arg = (seq_con_arg*) malloc(sizeof(*arg));
        arg->f = &f;
        arg->q = &qs[i];
        pthread_create(&threads[i], NULL,  seq_con_job, arg);
    }


    for (int i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    map<pthread_t, set<int> > accounted;
    for (int i = 0; i < NUM_THREADS; i++) {
        map<pthread_t, int> order;
        optional<pair<pthread_t,int> > got;
        while ((got = qs[i].deq()).valid) {
            auto val = got.value;
            assert(val.second <= NITEMS);
            assert(val.second > 0);
            if (order[val.first] == val.second) {
                printf("%i => %i from %lu according to %lu\n", order[val.first], val.second, val.first,threads[i]);
            }
            assert(order[val.first] != val.second);
            assert(order[val.first] < val.second);
            order[val.first] = val.second;
            assert(!accounted[val.first].count(val.second));
            accounted[val.first].insert(val.second);
        }
    }
    assert(!f.deq(m));
    for (int i = 0; i < NUM_THREADS; i++) {
        for (int j = 1; j <= NITEMS; j++) {
            assert(accounted[threads[i]].count(j));
        }
    }
}
#ifndef NTESTS
#define NTESTS 15
#endif
int main() {
    basic_test();
    for (int i = 0; i < NTESTS; i++) {
        paraput_test();
    }
    for (int i = 0; i < NTESTS; i++) {
        para_test();
    }
    for (int i = 0; i < NTESTS; i++) {
        seq_con_test();
    }
    return 0;
}
