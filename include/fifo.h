#include "optional.h"

#include <algorithm>
using std::remove_if;
#include <atomic>
using std::atomic;
#include <set>
using std::set;
#include <vector>
using std::vector;

#ifndef NULL
#define NULL 0
#endif

template <typename T, size_t MAX_THREADS=32> class fifo {
    // both dynamically allocated structures extend this
    struct dynamic {
            const bool is_opdesc;
        protected:
            inline dynamic(bool _is_opdesc)
            :   is_opdesc(_is_opdesc)
            {
            }
    };
    struct opdesc;
    struct account;
    struct node : dynamic {
        const T value;
        atomic<node*> next;
        atomic<opdesc*>* const enq_op_source;
        atomic<atomic<opdesc*>*> deq_op_source;
        opdesc* enq_op;
        atomic<opdesc*> deq_op;
        node(T _value, atomic<opdesc*>* _enq_op_source)
        :   dynamic(false)
        ,   value(_value)
        ,   next(NULL)
        ,   enq_op_source(_enq_op_source)
        ,   deq_op_source(NULL)
        ,   enq_op(NULL)
        ,   deq_op(NULL)
        {
        };
        node(atomic<opdesc*>* _enq_op_source)
        :   dynamic(false)
        ,   value()
        ,   next(NULL)
        ,   enq_op_source(_enq_op_source)
        ,   deq_op_source(NULL)
        ,   enq_op(NULL)
        ,   deq_op(NULL)
        {
        };
    };
    struct opdesc : dynamic {
        bool pending;
        bool is_deq;
        node* proposed;
        opdesc(bool _pending, bool _is_deq, node* _proposed)
        :   dynamic(true)
        ,   pending(_pending)
        ,   is_deq(_is_deq)
        ,   proposed(_proposed)
        {
        }
        opdesc()
        :   dynamic(true)
        {
        };
    };

    atomic<node*> head, tail;
    #define NUM_HAZARDS 4
    struct account {
        atomic<opdesc*> op_source;
        atomic<unsigned long> phase_source;
        dynamic* align1[6]; // cache coherence padding
        atomic<dynamic*> hazards[NUM_HAZARDS];
        vector<dynamic*>* rlist;
        dynamic* align2[3]; // cache coherence padding
        account() {
            op_source.store(NULL);
            phase_source.store(0);
            for (unsigned long i = 0; i < NUM_HAZARDS; i++) {
                hazards[i].store(NULL);
            }
            rlist = new vector<dynamic*>;
        }
        ~account() {
            for (auto it = rlist->cbegin(); it != rlist->cend(); it++) {
                if ((*it)->is_opdesc) {
                    delete *it;
                } else {
                    node* nd = (node*) *it;
                    delete nd->enq_op;
                    delete nd->deq_op.load();
                    delete nd;
                }
            }
            delete rlist;
        }
        void retire(fifo* f, dynamic* st) {
            rlist->push_back(st);
            if (rlist->size() > MAX_THREADS * NUM_HAZARDS) {
                scan(f);
            }
        }
    private:
        void scan(fifo* f) {
            set<dynamic*> plist;
            unsigned long i = f->n_subscribed.load(std::memory_order_relaxed);
            while (i --> 0) {
                for (unsigned long j = 0; j < NUM_HAZARDS; j++) {
                    dynamic* hptr = f->accounts[i].hazards[j];
                    if (hptr) {
                        plist.insert(hptr);
                    }
                }
            }
            auto end = remove_if(rlist->begin(), rlist->end(),
                [plist] (dynamic* ptr) -> bool {
                    if (ptr->is_opdesc) {
                        if (plist.count(ptr)) {
                            return false;
                        }
                        delete ptr;
                    } else {
                        node* nd = (node*) ptr;
                        if (plist.count(nd)
                         || plist.count(nd->enq_op)
                         || !nd->deq_op.load()
                         || plist.count(nd->deq_op.load())
                        ) {
                            return false;
                        }
                        delete nd->enq_op;
                        delete nd->deq_op.load();
                        delete nd;
                    }
                    return true;
                }
            );
            rlist->resize(end - rlist->begin());
        }
    } accounts[MAX_THREADS];
    atomic<unsigned long> n_subscribed;
    
    atomic<unsigned long> next_phase;
    long getNextPhase() {
        return next_phase.fetch_add(1);
    }

    bool isStillPending(atomic<unsigned long>* phase_source, opdesc* currDesc, unsigned long ph) {
        if (!currDesc) {
            return false;
        }
        if (!currDesc->pending) {
            return false;
        }
        return isStillPending(phase_source, ph);
    }
    inline bool isStillPending(atomic<unsigned long>* phase, unsigned long ph) {
        return phase->load() <= ph;
    }
    void help_enq(account* me, account* them, opdesc* currDesc) {
        atomic<unsigned long>* phase_source = &them->phase_source;
        atomic<opdesc*>* op_source = &them->op_source;
        while (op_source->load() == currDesc) {
            node* last = tail.load();
            me->hazards[0].store(last);
            if (last != tail.load()) {
                continue;
            }
            node *next = last->next.load();
            me->hazards[1].store(next); // do not have to check because it is write-once
            if (last != tail.load()) {
                continue;
            }
            if (next) {
                // help an enq in progress
                help_finish_enq(me);
                continue;
            }
            // enqueue can be applied
            if (op_source->load() != currDesc) {
                // has already been applied
                return;
            }
            node* proposed = currDesc->proposed;
            if (last->next.compare_exchange_strong(next, proposed)) {
                // consider doing this unconditionally
                help_finish_enq(me);
            }
        }
    }
    void help_finish_enq(account* me) {
        node *last
        ,    *next;
        atomic<opdesc*>* op_source;
        opdesc *currDesc;

        last = tail.load();
        me->hazards[0].store(last);
        if (last != tail.load()) {
            goto finish;
        }
        next = last->next.load();
        if (!next) {
            goto finish;
        }
        me->hazards[1].store(next);
        if (last != tail.load()) {
            goto finish;
        }
        op_source = next->enq_op_source;
        currDesc =  op_source->load();
        me->hazards[2].store(currDesc);
        if (currDesc != op_source->load()) {
            goto finish;
        }
        if (last != tail.load()) {
            goto finish;
        }
        if (currDesc && currDesc->pending && currDesc->proposed == next) {
            op_source->compare_exchange_strong(currDesc, NULL);
        }
        tail.compare_exchange_strong(last, next);

    finish:
        me->hazards[2].store(NULL);
        me->hazards[1].store(NULL);
        me->hazards[0].store(NULL);

    }
    void help_finish_deq(account* me) {
        node *first, *next, *saved_first;
        atomic<opdesc*> *op_source;
        opdesc *currDesc;
        first = head.load();
        me->hazards[0].store(first);
        if (first != head.load()) {
            goto finish;
        }
        next = first->next.load();
        if (!next) {
            goto finish;
        }
        me->hazards[1].store(next);
        if (first != head.load()) {
            goto finish;
        }
        op_source = next->deq_op_source.load();
        if (!op_source) {
            goto finish;
        }
        if (first != head.load()) {
            goto finish;
        }
        currDesc = op_source->load();
        me->hazards[2].store(currDesc);
        if (op_source->load() != currDesc) {
            goto update;
        }
        if (currDesc && currDesc->pending && currDesc->is_deq && currDesc->proposed == first) {
            opdesc *newDesc = new opdesc(false, true, next);
            if (op_source->compare_exchange_strong(currDesc, newDesc)) {
                me->retire(this, currDesc);
            } else {
                delete newDesc;
            }
        }
    update:
        saved_first = first;
        if (head.compare_exchange_strong(first, next)) {
            me->retire(this, saved_first);
        }
    finish:
        me->hazards[2].store(NULL);
        me->hazards[1].store(NULL);
        me->hazards[0].store(NULL);
    }
    void help_deq(account* me, account* acc, opdesc* currDesc, unsigned long phase) {
        atomic<unsigned long>* phase_source = &acc->phase_source;
        atomic<opdesc*>* op_source = &acc->op_source;

        while (isStillPending(phase_source, currDesc, phase)) {
            node *first = head.load();
            me->hazards[0].store(first);
            if (first != head.load()) {
                continue;
            }
            node *last = tail.load();
            me->hazards[1].store(last);
            if (last != tail.load()) {
                continue;
            }
            node *next = first->next.load();
            me->hazards[2].store(next);
            if (first != head.load()) {
                continue;
            }
            if (first == last) {
                // queue might be empty
                if (next) {
                    // some enqueue is in progress
                    // help it first, then retry
                    help_finish_enq(me);
                    continue;
                }
                // queue is empty
                currDesc = op_source->load();
                me->hazards[3].store(currDesc);
                if (currDesc != op_source->load()) {
                    continue;
                }
                if (last == tail.load() && isStillPending(phase_source, currDesc, phase)) {
                    if (op_source->compare_exchange_strong(currDesc, NULL)) {
                        me->retire(this, currDesc);
                    }
                }
            } else {
                if (next->deq_op_source.load()) {
                    // finish someone else's deq first
                    help_finish_deq(me);
                    continue;
                }
                // queue is not empty
                opdesc *currDesc = op_source->load();
                me->hazards[3].store(currDesc);
                if (currDesc != op_source->load()) {
                    continue;
                }
                if (!isStillPending(phase_source, currDesc, phase)) {
                    break;
                }
                if (first == head.load() && first != currDesc->proposed) {
                    // precommit
                    dynamic* const previous = currDesc;
                    opdesc* current = new opdesc(true, true, first);
                    if (op_source->compare_exchange_strong(currDesc, current)) {
                        me->retire(this, previous);
                    } else {
                        delete current;
                        continue;
                    }
                }
                atomic<opdesc*>* expected = NULL;
                next->deq_op_source.compare_exchange_strong(expected, op_source);
                help_finish_deq(me);
            }
        }
    }
    void help(account* me, unsigned long phase) {
        unsigned long n = n_subscribed.load(std::memory_order_relaxed);
        for (unsigned long index = 0; index < n; index++) {
            account* them = &accounts[index];
            atomic<opdesc*>* op_source = &them->op_source;
            opdesc *currDesc, *after;
            unsigned long fail_count = 0;
            currDesc = op_source->load();
            do {
                me->hazards[3].store(currDesc);
                after = op_source->load();
                if (currDesc == after) {
                    break;
                }
                unsigned long threadCount = n_subscribed.load();
                if (fail_count++ >= (threadCount * threadCount + threadCount)/2) {
                    /*
                        This can not fail more than threads^2 times without
                        some thread having completed an operation for thread (index) in the
                        lifetime of this help() function
                        The number of times that a 'precommit' can cause a failure is
                        (N-1) + (N-2) + (N-3) + ... + 2 + 1 = (N^2 + N)/2
                        The number of times that a deq can be committed is 1.
                        Once a deq is committed it is completed.
                        Therefore a ref-read of a deq operation can fail up to (N^2 + N)/2 + 1 times without success.
                        An enq operation that fails here has succeeded.
                    */
                    return;
                }
                currDesc = after;
            } while (1);

            if (isStillPending(&them->phase_source, currDesc, phase)) {
                if (currDesc->is_deq) {
                    help_deq(me, them, currDesc, phase);
                } else {
                    help_enq(me, them, currDesc);
                }
            }
        }
    }
public:
    fifo()
    {
        node *sentinal = new node(NULL);
        sentinal->enq_op = new opdesc;
        sentinal->deq_op.store(new opdesc);
        head.store(sentinal);
        tail.store(sentinal);
        next_phase.store(0);
        n_subscribed.store(0);
    }

    ~fifo() {
        node* remaining = head.load();
        delete remaining->deq_op.load();
        do {
            node* to_rm = remaining;
            delete to_rm->enq_op;
            remaining = to_rm->next;
            delete to_rm;
        } while (remaining);
    }

    void enq(account* me, T value) {
        unsigned long phase = getNextPhase();
        me->phase_source.store(phase);
        node *proposing = new node(value, &me->op_source);
        opdesc *enq_op = new opdesc(true, false, proposing); 
        proposing->enq_op = enq_op;
        me->op_source.store(enq_op);
        help(me, phase);
    }

    optional<T> deq(account* me) {
        unsigned long phase = getNextPhase();
        me->phase_source.store(phase);

        optional<T> ret;

        opdesc* request = new opdesc(true, true, NULL);
        me->op_source.store(request);
        help(me, phase);
        opdesc* deqop = me->op_source.load();
        if (!deqop) {
            ret.valid = false;
            return ret;
        }
        help_finish_deq(me);
        me->op_source.store(NULL);
        node* deqd = deqop->proposed;
        ret.valid = true;
        ret.value = deqd->value;
        deqd->deq_op.store(deqop);
        return ret;
    }
    account* subscribe() {
        unsigned long index = n_subscribed.fetch_add(1, std::memory_order_relaxed);
        account* me = &accounts[index];
        return me;
    }
};
