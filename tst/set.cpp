#include "set.h"
#include "utils.h"

#include <assert.h>

#ifndef REPS
#define REPS 1000
#endif

void test_setof(size_t ports) {
    set<int> one(ports);
    for (int n = -1; n > -30; n--) {
        for (int i = 0; i < 30; i++) {
            assert(not one.has(i));
        }
        for (int i = -1; i > n; i--) {
            assert(one.has(i));
        }
        assert(one.add(n));
        assert(one.has(n));
        for (int i = 0; i < 30; i++) {
            assert(not one.has(i));
        }
        for (int i = -1; i >= n; i--) {
            assert(one.has(i));
        }
        assert(one.remove(n));
        assert(not one.has(n));
        for (int i = 0; i < 30; i++) {
            assert(not one.has(i));
        }
        for (int i = -1; i > n; i--) {
            assert(one.has(i));
        }
        assert(one.add(n));
        assert(one.has(n));
        for (int i = 0; i < 30; i++) {
            assert(not one.has(i));
        }
        for (int i = -1; i >= n; i--) {
            assert(one.has(i));
        }
        assert(not one.add(n));
        assert(one.has(n));
        for (int i = 0; i < 30; i++) {
            assert(not one.has(i));
        }
        for (int i = -1; i >= n; i--) {
            assert(one.has(i));
        }
    }
}
void test_setsingle() {
    for (int i = 1; i < 100; i++) {
        test_setof(i);
    }
}
struct para_add_map_param {
    set<int>* dest;
    int me;
    int step;
    size_t size;
};
void* para_add_map(void* param) {
    struct para_add_map_param* arg = (struct para_add_map_param*) param;
    pthread_t helpers[SCION_ARITY];
    for (size_t i = 0; i < SCION_ARITY; i++) {
        size_t offset = (i+1) * arg->step + arg->me;
        if (offset < arg->size) {
            struct para_add_map_param* delegate = (struct para_add_map_param*) malloc(sizeof(struct para_add_map_param));
            delegate->dest = arg->dest;
            delegate->step = arg->step * SCION_ARITY;
            delegate->size = arg->size;
            delegate->me = offset;
            pthread_create(helpers + i, NULL, para_add_map, delegate);
        }
    }
    arg->dest->add(arg->me);
    for (size_t i = 0; i < SCION_ARITY; i++) {
        size_t offset = (i+1) * arg->step + arg->me;
        if (offset < arg->size) {
            pthread_join(helpers[i], NULL);
        }
    }
    free(param);
    return EXIT_SUCCESS;
}
void test_paraaddof(size_t throughput) {
    unsigned int reps = REPS / 100;
    if (not reps) {
        reps = 1;
    }
    for (unsigned int test = 1; test <= reps; test++) {
        // TODO print progress
        const unsigned int size = 250;
        set<int> dest(throughput);
        struct para_add_map_param* me = (struct para_add_map_param*) malloc(sizeof(struct para_add_map_param));
        me->me = 0;
        me->size = size;
        me->step = 1;
        me->dest = &dest;
        para_add_map(me);
        for (size_t i = 0; i < size; i++) {
            assert(dest.has(i));
        }
    }
}
struct multiaddparam {
    int offset;
    int size;
    int arity;
    set<int>* dest;
};
void* add_range(void* param) {
    struct multiaddparam const*const arg = (struct multiaddparam const*const)param;
    for (int i = arg->offset; i < arg->size; i+=arg->arity) {
        assert(arg->dest->add(i));
    }
    free(param);
    return NULL;
}
void* rm_range(void* param) {
    struct multiaddparam const*const arg = (struct multiaddparam const*const)param;
    for (int i = arg->offset; i < arg->size; i+=arg->arity) {
        assert(arg->dest->remove(i));
    }
    free(param);
    return NULL;
}
void test_multichangeof(size_t throughput) {
    unsigned int reps = REPS;
    if (not reps) {
        reps = 1;
    }
    for (unsigned int test = 1; test <= reps; test++) {
        printf("%04X / %04X", test, reps);
        for (size_t i = 0; i < 11; i++) {
            putchar('\b');
        }
        const int arity = 6;
        pthread_t workers[arity];
        const int size = 250;
        set<int> dest(throughput);
        for (int offset = 0; offset < arity; offset++) {
            struct multiaddparam* delegate = (struct multiaddparam*) malloc(sizeof(struct multiaddparam));
            delegate->arity = arity;
            delegate->size = size;
            delegate->offset = offset;
            delegate->dest = &dest;
            pthread_create(&workers[offset], NULL, add_range, delegate);
        }
        for (int offset = 0; offset < arity; offset++) {
            pthread_join(workers[offset], NULL);
            for (int i = offset; i < size; i+= arity) {
                assert(dest.has(i));
            }
            struct multiaddparam* delegate = (struct multiaddparam*) malloc(sizeof(struct multiaddparam));
            delegate->arity = arity;
            delegate->size = size;
            delegate->offset = offset;
            delegate->dest = &dest;
            pthread_create(&workers[offset], NULL, rm_range, delegate);
        }
        for (int offset = 0; offset < arity; offset++) {
            pthread_join(workers[offset], NULL);
            for (int i = offset; i < size; i+= arity) {
                assert(not dest.has(i));
            }
            struct multiaddparam* delegate = (struct multiaddparam*) malloc(sizeof(struct multiaddparam));
            delegate->arity = arity;
            delegate->size = size;
            delegate->offset = offset;
            delegate->dest = &dest;
            pthread_create(&workers[offset], NULL, add_range, delegate);
        }
        for (int offset = 0; offset < arity; offset++) {
            pthread_join(workers[offset], NULL);
            for (int i = offset; i < size; i+= arity) {
                assert(dest.has(i));
            }
        }
    }
    for (size_t i = 0; i < 11; i++) {
        putchar(' ');
    }
    for (size_t i = 0; i < 11; i++) {
        putchar('\b');
    }
}
struct addrm_param {
    set<int>* shared;
    size_t times;
};
void* addrm_rm(void* arg) {
    struct addrm_param* param = (struct addrm_param*) arg;
    size_t ret = 0;
    for (size_t i = 0; i < param->times; i++) {
        if (param->shared->remove(0)) {
            ret+=1;
        }
    }
    return (void*) ret;
}
void* addrm_add(void* arg) {
    struct addrm_param* param = (struct addrm_param*) arg;
    size_t ret = 0;
    for (size_t i = 0; i < param->times; i++) {
        if (param->shared->add(0)) {
            ret+=1;
        }
    }
    return (void*) ret;
}
size_t test_addrm_count(size_t times) {
    struct addrm_param param;
    pthread_t children[2];
    param.times = times;
    param.shared = new set<int>(1);
    pthread_create(&children[0], NULL, addrm_rm, &param);
    pthread_create(&children[1], NULL, addrm_add, &param);
    size_t rms;
    size_t adds;
    pthread_join(children[0], (void**)&rms);
    pthread_join(children[1], (void**)&adds);
    if (param.shared->remove(0)) {
        rms += 1;
    }
    assert(rms == adds);
    delete param.shared;
    return rms;
}

int main() {
    test_setsingle();
    test_paraaddof(1);
    test_paraaddof(100);
    test_multichangeof(1);
    test_multichangeof(500);
    size_t changes = test_addrm_count(20000);
    assert(changes <= 20000);
    return 0;

}
