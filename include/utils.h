#ifndef scion_utils_h
#define scion_utils_h

#include <cstdlib>
#include <pthread.h>
#include "stream.h"

// logarithm base for map
// on futuristically parallel systems, should be closer to 2
// on less-parallel systems, should be higher
const size_t SCION_ARITY = 2;
// apply a function to everything in the array
template <typename A, typename B> B** map(A** in, B* (*func) (A*), size_t size); // O(log n)
// apply a function to a selection of streams, producing a new stream
// for the identity function, this merges the streams
template <typename A, typename B> key<B>** map_stream(key<A>** ins, B (*func) (A*), size_t in_count, size_t out_count); 

template <typename A,typename B> struct map_helper_param {
    A** in;
    B** out;
    B* (*func)(A*);
    size_t size;
    size_t step;
};
template <typename A, typename B> void* map_helper(void* param) {
    struct map_helper_param<A,B>* arg = (struct map_helper_param<A,B>*) param;
    pthread_t helpers[SCION_ARITY];
    for (size_t i = 0; i < SCION_ARITY; i++) {
        size_t offset = (i+1) * (arg->step);
        if (offset < arg->size) {
            struct map_helper_param<A,B>* delegate = (struct map_helper_param<A,B>*) malloc(sizeof(struct map_helper_param<A,B>));
            delegate->in   = arg->in + offset;
            delegate->out  = arg->out + offset;
            delegate->size = arg->size - offset;
            delegate->step = arg->step * SCION_ARITY;
            delegate->func = arg->func;
            pthread_create(helpers + i, NULL, map_helper<A,B>, delegate);
        }
    }
    *arg->out = arg->func(*arg->in);
    for (size_t i = 0; i < SCION_ARITY; i++) {
        size_t offset = (i+1) * (arg->step);
        if (offset < arg->size) {
            pthread_join(helpers[i], NULL);
        }
    }
    free(param);
    return EXIT_SUCCESS;
}
template <typename A, typename B> B** map(A** in, B* (*func) (A*), size_t size) {
    B** ret = (B**) malloc(sizeof(B*) * size);
    if (size) {
        struct map_helper_param<A,B>* arg = (struct map_helper_param<A,B>*) malloc(sizeof(struct map_helper_param<A,B>));
        arg->out  = ret;
        arg->in   = in;
        arg->size = size;
        arg->func = func;
        arg->step = 1;
        map_helper<A,B>(arg);
    }
    return ret;
}
template <typename A, typename B> struct map_stream_param {
    key<A>** ins;
    size_t in_count;
    stream<B>* out;
    B (*func) (A);
};
template <typename A, typename B> void* map_stream_worker(void* param) {
    struct map_stream_param<A,B>* arg = (struct map_stream_param<A,B>*) param;
    size_t depleted;
    while (true) {
        depleted = 0;
        for (size_t i = 0; i < arg->in_count; i++) {
            bool was_closed = closed(arg->ins[i]);
            while (ready(arg->ins[i])) {
                arg->out->put(arg->func(from(arg->ins[i])));
            }
            if (was_closed) {
                depleted++;
            }
        }
        if (depleted < arg->in_count) {
            sleep(0);
        } else {
            break;
        }
    }
    arg->out->close();
    return EXIT_SUCCESS;
}
template <typename A, typename B> key<B>** map_stream(key<A>** ins, B (*func) (A), size_t in_count, size_t out_count) {
    stream<B>* out = new stream<B>(out_count);
    key<B>** ret = (key<B>**) malloc(sizeof(key<B>*) * out_count);
    pthread_t worker;
    for (size_t i = 0; i < out_count; i++) {
        ret[i] = out->listen();
    }
    struct map_stream_param<A,B>* param = (struct map_stream_param<A,B>*) malloc(sizeof(map_stream_param<A,B>));
    param->ins = ins;
    param->out = out;
    param->in_count = in_count;
    param->func = func;
    pthread_create(&worker, NULL, map_stream_worker<A,B>, param);
    pthread_detach(worker);
    return ret;
}
#endif
