#ifndef scion_optional
#define scion_optional
template <typename V> struct optional {
    V value;
    bool valid;
    inline operator bool() const {
        return valid;
    };
    inline operator V() const {
        return value;
    }
    inline V operator *() const {
        return value;
    };
    inline V& operator *() {
        return value;
    };
    inline V* operator->() {
        return &value;
    }
    inline optional(bool _valid)
    :   valid(_valid)
    {
    }
    inline optional(V _value)
    :   value(_value)
    ,   valid(true)
    {
    }
    inline optional() {}
};
template <> struct optional<bool> {
    bool value;
    bool valid;
    inline operator bool() const {
        return valid;
    }
    inline bool operator *() const {
        return value;
    }
    inline bool& operator *() {
        return value;
    }
    inline optional() {};
    inline optional(bool _value)
    :   value(_value)
    ,   valid(true)
    {
    }
    inline optional(bool _valid, bool _value)
    :   value(_value)
    ,   valid(_valid)
    {
    }
};
#endif
