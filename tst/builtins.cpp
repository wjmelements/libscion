#include "builtins.h"

#include <assert.h>


void test_stdout() {
    pthread_t out_thread;
    stream<char>* out = new stream<char>(1);   
    key<char>* out_key = out->listen();
    pthread_create(&out_thread, NULL, STDOUT, out_key);
    char* message = "pass: stdout\n";
    for (size_t index = 0; message[index]; index++) {
        out->put(message[index]);
    }
    // TODO ensure message was actually written to stdout
    out->close();
    pthread_join(out_thread, NULL);
    delete out;
    free(out_key);
}
// ensure that reading from in reads everything from stdin
void test_stdin() {
    pthread_t in_thread;
    stream<char>* in = new stream<char>(1);
    key<char>* in_id = in->listen();
    pthread_create(&in_thread, NULL, STDIN, in);
    while (!depleted(in_id)) {
        assert(ready(in_id));
        char next = from(in_id);
        putchar(next);
    }
    assert(closed(in_id));
    // TODO ensure message was identical to message in makefile
    pthread_join(in_thread, NULL);
    delete in;
    delete in_id;
}
// ensure args read in correctly
void test_argv(int argc, char* argv[]) {
    argv_args args_argv;
    args_argv.arg_stream = new stream<char*>(1);
    args_argv.argc = argc;
    args_argv.argv = argv;
    key<char*>* args_id = args_argv.arg_stream->listen();
    pthread_t arg_thread;
    pthread_create(&arg_thread, NULL, ARGV, &args_argv);
    for (int i = 1; i < argc; i++) {
        char* arg = from(args_id);
        for (int j = 0; argv[i][j]; j++) {
            assert(argv[i][j] == arg[j]);
        }
    }
    assert(depleted(args_id));
    assert(closed(args_id));
    pthread_join(arg_thread, NULL);
    delete args_argv.arg_stream;
    free(args_id);
}
// ensure files read correctly
void test_readfile() {
    pthread_t read_thread;
    read_file_args rfargs;
    rfargs.path = "include/stream.h";
    stream<char*>* lines = new stream<char*>(1);
    key<char*>* lines_id = lines->listen();
    rfargs.lines = lines;
    pthread_create(&read_thread, NULL, read_file, &rfargs);
    FILE* file = fopen(rfargs.path, "r");
    while (!depleted(lines_id)) {
        char* line = from(lines_id);
        for (size_t i = 0; line[i]; i++) {
            char c = fgetc(file);
            assert(line[i] == c);
        }
    }
    assert(closed(lines_id));
    pthread_join(read_thread, NULL);
    delete lines;
    free(lines_id);
}
// test read directory functionality by comparing output with ls -f1
void test_readdir() {
    pthread_t dir_thread;
    read_dir_args rdargs;
    rdargs.filenames = new stream<char*>(1);
    rdargs.path = ".";
    key<char*>* filenames_id = rdargs.filenames->listen();
    int ret;
    ret = system("ls -f1 > tmpscion.out");
    assert(ret == 0);
    FILE* tmpscion = fopen("tmpscion.out", "r");
    pthread_create(&dir_thread, NULL, read_dir, &rdargs);
    while(!depleted(filenames_id)) {
        char* filename_from = from(filenames_id);
        char* filename_ls = (char*) malloc(500);
        filename_ls = fgets(filename_ls, 500, tmpscion);
        int i;
        for (i = 0; filename_ls[i] && filename_from[i]; ++i) {
            assert(filename_ls[i] == filename_from[i]);
        }
    }
    assert(closed(filenames_id));
    assert(fgets(NULL, 500, tmpscion) == NULL);
    fclose(tmpscion);
    pthread_join(dir_thread, NULL);
    ret = system("rm -f tmpscion.out");
    assert(ret == 0);
    delete rdargs.filenames;
    free(filenames_id);
}
int main(int argc, char* argv[]) {
    test_stdout();
    test_stdin();
    test_argv(argc, argv);
    test_readfile();
    test_readdir();
    return 0;
}
