#include <algorithm>
using std::remove_if;
#include <atomic>
using std::atomic;
#include <set>
using std::set;
#include <vector>
using std::vector;

template<size_t NUM_THREADS=64, size_t NUM_HAZARDS=5> class dynamic {
protected:
    struct account {
        atomic<const void*> hazards[NUM_HAZARDS];
        vector<void*> retired;
        template <unsigned index=0> void announce(const void *p) {
            static_assert(index < NUM_HAZARDS, "announcing a pointer of too high an index");
            hazards[index].store(p, std::memory_order_release);
        }
    } accounts[NUM_THREADS];
    void retire(account* m, void *p) {
        m->retired.push_back(p);
        if (m->retired.size() > NUM_THREADS * NUM_HAZARDS) {
            scan(m);
        }
    }
private:
    atomic<size_t> num_subscribed;
    void scan(account* m) {
        set<const void*> held;
        size_t N = num_subscribed.load();
        for (size_t i = 0; i < N; i++) {
            for (size_t j = 0; j < NUM_HAZARDS; j++) {
                const void* p = accounts[i].hazards[j].load(std::memory_order_consume);
                if (p) {
                    held.insert(p);
                }
            }
        }
        auto begin = m->retired.begin();
        auto end = remove_if(begin, m->retired.end(),
            [held](void* p) -> bool {
                if (held.count(p)) {
                    return false;
                }
                free(p);
                return true;
            }
        );
        m->retired.resize(end - begin);
    }
protected:
    dynamic()
    :   num_subscribed(0)
    {
    }
    ~dynamic() {
        size_t N = num_subscribed.load(std::memory_order_relaxed);
        for (size_t i = 0; i < N; i++) {
            for (auto it = accounts[i].retired.cbegin(); it != accounts[i].retired.cend(); it++) {
                free(*it);
            }
        }
    }
public:
    account* subscribe() {
        size_t i = num_subscribed.fetch_add(1, std::memory_order_relaxed);
        return &accounts[i];
    }
};
