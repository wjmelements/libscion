#include "stream.h"
#include "stdio.h"
#include "dirent.h"
#include "builtins.h"
void* STDOUT(void* arg) {
    key<char>* out = (key<char>*) arg;
    while(!depleted(out)) {
        char next = from(out);
        putchar(next);
    }
    return EXIT_SUCCESS;
}
void* STDERR(void* arg) {
    key<char>* err = (key<char>*) arg;
    while(!depleted(err)) {
        char next = from(err);
        putc(next, stderr);
    }
    return EXIT_SUCCESS;
}
void* STDIN(void* arg) {
    stream<char>* in = (stream<char>*) arg;
    while(1) {
        char next = getchar();
        if (next == EOF) {
            in->close();
            return EXIT_SUCCESS;
        }
        in->put(next);
    }
}
void* ARGV(void* args_argv) {
    argv_args* args = (argv_args*) args_argv;
    stream<char*>* sys_args = args->arg_stream;
    int argc = args->argc;
    char** argv = args->argv;
    for (int i = 1; i < argc; ++i) {
        sys_args->put(argv[i]);
    }
    sys_args->close();
    return EXIT_SUCCESS;
}
void* read_file(void* rfargs) {
    read_file_args* args = (read_file_args*) rfargs;
    char* path = args->path;
    stream<char*>* lines = args->lines;
    FILE* file = fopen(path, "r");
    // TODO file does not exist
    size_t length = 0;
    stream<char> linked_buffer(1);
    key<char>* key = linked_buffer.listen();
    while (1) {
        int character = fgetc(file);
        if (character != EOF) {
            length++;
            linked_buffer.put(character);
        }
        if (character == '\n' || character == EOF) {
            char* line = (char*) malloc((length + 1) * sizeof(char));
            for (size_t i = 0; i < length; i++) {
                line[i] = from(key); // used to cast (char)
            }
            line[length] = 0;
            length = 0;
            lines->put(line);
        }
        if (character == EOF) {
            lines->close();
            return EXIT_SUCCESS;
        }
    }
}
void* read_dir(void* args) {
    read_dir_args* rdargs = (read_dir_args*) args;
    DIR* dir = opendir(rdargs->path);
    struct dirent* entry = readdir(dir);
    while (entry) {
        char* filename = entry->d_name;
        rdargs->filenames->put(filename);
        entry = readdir(dir);
    }
    rdargs->filenames->close();
    closedir(dir);
    return EXIT_SUCCESS;
}
