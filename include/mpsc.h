#ifndef IUMPSC
#define IUMPSC

#include <atomic>
using std::atomic;
#include <optional.h>
#include <stdint.h>
#include <stdlib.h>

template <typename T,size_t N> struct mpsc {
private:
    atomic<uint64_t> seq;
    #define scion_spec
    #define scion_seq
    #include <spsc.h>
    #undef scion_seq
    #undef scion_spec
    spsc queues[N];
public:
    mpsc() {
        for (size_t i = 0; i < N; i++) {
            queues[i].setSeq(&seq);
        }
    };
    ~mpsc() { };
    void enq(T item, int id) {
        queues[id].enq(item);
    };
    const optional<T> deq() {
        uint64_t minarg = -1;
        uint64_t min = -1;
        // TODO priority queue
        for (size_t i = 0; i < N; i++) {
            optional<uint64_t> peeked = queues[i].peekSeq();
            if (peeked.valid && peeked.value < min) {
                minarg = i;
            }
        }
        if (minarg == (uint64_t)-1) {
            optional<T> ret;
            ret.valid = false;
            return ret;
        }
        return queues[minarg].deq();
    };
};
#endif
