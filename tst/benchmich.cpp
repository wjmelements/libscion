//0m40.910s
#include "cds/container/michael_kvlist_hp.h"
#include "cds/container/michael_map.h"
#include <cds/init.h>
using cds::container::MichaelHashMap;
#include <pthread.h>

#ifndef N_THREADS
#define N_THREADS 8
#endif

#ifndef N_ITEMS
#define N_ITEMS 5000000
#endif
struct list_traits: public cds::container::michael_list::type_traits
{
    typedef std::less<int>      less ;
};
typedef cds::container::MichaelKVList< cds::gc::HP, long, long, list_traits> long2long_list;
struct map_traits: public cds::container::michael_map::type_traits {
    struct hash {
        size_t operator()( long i ) const
        {
            return cds::opt::v::hash<long>()( i ) ;
        }
    };
};
typedef MichaelHashMap<cds::gc::HP, long2long_list, map_traits > dict;
struct worker_arg {
    dict* d;
    int tid;
    worker_arg
    (   dict* _d
    ,   int _tid
    ):  d(_d)
    ,   tid(_tid)
    {}
};
void* worker(void* va) {
    cds::threading::Manager::attachThread();
    struct worker_arg* wa = (struct worker_arg*) va;
    dict* map = wa->d;
    int tid = wa->tid;
    delete wa;

    for (long i = 0; i < N_ITEMS; i++) {
        map->insert(i + tid * N_ITEMS);
    }
    for (long j = 0; j < N_THREADS; j++) {
        for (long i = 0; i < N_ITEMS; i++) {
            long key = i + j * N_ITEMS;
            long got = map->find(key);
            map->insert(key);
        }
    }

    cds::threading::Manager::detachThread();
    return NULL;
}

int main() {
    cds::Initialize(0);
    cds::gc::HP hpGC;
    cds::threading::Manager::attachThread();
    {
        dict d(N_ITEMS, 5);
        pthread_t threads[N_THREADS];
        for (int i = 0; i < N_THREADS; i++) {
            pthread_create(&threads[i], NULL,  worker, new worker_arg(&d, i));
        }
        for (int i = 0; i < N_THREADS; i++) {
            pthread_join(threads[i], NULL);
        }
    }
    cds::threading::Manager::detachThread();
    cds::Terminate();
    return 0;
}
