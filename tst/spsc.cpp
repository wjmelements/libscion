#include <spsc.h>

#include <assert.h>
#include <pthread.h>

void test_ops() {
    spsc<int> one;
    assert(!one.deq());
    one.enq(1);
    optional<int> got = one.deq();
    assert(got);
    assert(*got == 1);
    got = one.deq();
    assert(!got);
    one.enq(2);
    one.enq(1);
    got = one.deq();
    assert(got);
    assert(*got == 2);
    got = one.deq();
    assert(got);
    assert(*got == 1);
    assert(!one.deq());
}
#ifndef NITEMS
#define NITEMS 10000
#endif
static void *producer(void* varg) {
    spsc<int>* q = (spsc<int>*) varg;
    for (int i = 0; i < NITEMS; i++) {
        q->enq(i);
    }
    return NULL;
}
static void *consumer(void* varg) {
    spsc<int>* q = (spsc<int>*) varg;
    for (int i = 0; i < NITEMS; i++) {
        optional<int> got = q->deq();
        if (got) {
            assert(*got == i);
        } else {
            i--;
        }
    }
    return NULL;
}
void test_para() {
    spsc<int> q;
    pthread_t threads[2];
    pthread_create(&threads[0], NULL, producer, &q);
    pthread_create(&threads[1], NULL, consumer, &q);
    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);
}
#ifndef NTESTS
#define NTESTS 5
#endif
int main() {
    test_ops();
    for (int i = 0; i < NTESTS; i++) {
        test_para();
    }
    return 0;
}
