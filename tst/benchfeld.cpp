#define WaitFreeHashTable dict
#define useThreadWatch
#include "feldman/WaitFreeHashTable.h"

#include <pthread.h>

#ifndef N_THREADS
#define N_THREADS 8
#endif

#ifndef N_ITEMS
#define N_ITEMS (800000 / N_THREADS)
#endif

struct worker_arg {
    dict<long,long>* d;
    int tid;
    worker_arg
    (   dict<long,long>* _d
    ,   int _tid
    ):  d(_d)
    ,   tid(_tid)
    {}
};
void* worker(void* va) {
    struct worker_arg* wa = (struct worker_arg*) va;
    dict<long,long>* map = wa->d;
    int tid = wa->tid;
    delete wa;

    for (long i = 0; i < N_ITEMS; i++) {
        long key = i + tid * N_ITEMS;
        map->putIfAbsent(key, 0, tid);
    }
    for (long j = tid; j < tid + 16; j++) {
        long k = j % N_THREADS;
        for (long i = 0; i < N_ITEMS; i++) {
            long key = i + k * N_ITEMS;
            long got = map->get(key, tid);
            map->putUpdate(key, got, got+1, tid);
        }
    }
    return NULL;
}

int main() {
    dict<long, long> d(4, N_THREADS);
    pthread_t threads[N_THREADS];
    for (int i = 0; i < N_THREADS; i++) {
        pthread_create(&threads[i], NULL,  worker, new worker_arg(&d, i));
    }
    for (int i = 0; i < N_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}

