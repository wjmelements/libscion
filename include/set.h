#include <atomic>
using std::atomic;
#include <functional>
using std::hash;
#include <stdlib.h>

template <typename K> struct set_node;
template <typename K> class set {
public:
    set(size_t throughput);
    ~set();
    bool has(K key);
    bool add(K key);
    bool remove(K key);
protected:
    hash<K> hash_fn;
    size_t throughput;
    atomic<set_node<K>*>* table;
    inline size_t indexOf(K key);
    bool _has(K key, size_t index);
    bool _add(K key, size_t index);
    bool _remove(K key, size_t index);
};
template <typename K> struct set_node {
    K key;
    atomic<bool> has;
    atomic<struct set_node*> next;
};
template <typename K> set<K>::set(size_t throughput) {
    this->throughput = throughput;
    table = (atomic<set_node<K>*>*) calloc(this->throughput, sizeof(set_node<K>*));
}
template <typename K> set<K>::~set() {
    for (size_t i = 0; i < this->throughput; i++) {
        struct set_node<K>* curr = table[i];
        while (curr) {
            struct set_node<K>* prev = curr;
            curr = curr->next;
            free(prev);
        }
    }
    free(table);
}
template <typename K> size_t set<K>::indexOf(K key) {
    size_t h = hash_fn(key);
    h %= throughput;
    return h;
}
template <typename K> bool set<K>::has(K key) {
    size_t h = this->indexOf(key);
    return this->_has(key,h);
}
template <typename K> bool set<K>::_has(K key, size_t index) {
    set_node<K>* curr = table[index];
    while (curr) {
        if (curr->key == key) {
            return curr->has;
        } else {
            curr = curr->next;
        }
    }
    return false;
}
template <typename K> bool set<K>::add(K key) {
    size_t h = this->indexOf(key);
    return this->_add(key,h);
}
template <typename K> bool set<K>::_add(K key, size_t index) {
    atomic<set_node<K>*>* src = &table[index];
    set_node<K>* curr;
    while(true) {
        curr = *src;
        while (curr) {
            if (curr->key == key) {
                bool was = true;
                was = curr->has.exchange(was);
                return not was;
            }
            src = &curr->next;
            curr = src->load();
        }
        struct set_node<K> *add = (struct set_node<K>*)malloc(sizeof(struct set_node<K>));
        add->has = true;
        add->key = key;
        add->next = NULL;
        struct set_node<K> *expected = NULL;
        bool success = src->compare_exchange_strong(expected, add);
        if (success) {
            // add was successful
            return true;
        }
        free(add);
    }
}
template <typename K> bool set<K>::remove(K key) {
    size_t h = this->indexOf(key);
    return this->_remove(key, h);
}
template <typename K> bool set<K>::_remove(K key, size_t index) {
    atomic<set_node<K>*>* src = &table[index];
    set_node<K>* curr = *src;
    while (curr) {
        if (curr->key == key) {
            bool was = false;
            was = curr->has.exchange(was);
            return was;
        }
        src = &curr->next;
        curr = *src;
    }
    return false;
}
