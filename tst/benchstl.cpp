#define unordered_map dict
#include <unordered_map>
using std::unordered_map;
#undef map

#include <utility>
using std::make_pair;

#include <pthread.h>

#ifndef N_THREADS
#define N_THREADS 8
#endif

#ifndef N_ITEMS
#define N_ITEMS 5000000
#endif

struct worker_arg {
    dict<long,long>* d;
    pthread_mutex_t* m;
    int tid;
    worker_arg
    (   dict<long,long>* _d
    ,   pthread_mutex_t* _m
    ,   int _tid
    ):  d(_d)
    ,   m(_m)
    ,   tid(_tid)
    {}
};
void* worker(void* va) {
    struct worker_arg* wa = (struct worker_arg*) va;
    dict<long,long>* map = wa->d;
    pthread_mutex_t* mutex = wa->m;
    int tid = wa->tid;
    delete wa;

    for (long i = 0; i < N_ITEMS; i++) {
        pthread_mutex_lock(mutex);
        map->insert(make_pair(i + tid * N_ITEMS, tid));
        pthread_mutex_unlock(mutex);
    }
    for (long j = 0; j < N_THREADS; j++) {
        for (long i = 0; i < N_ITEMS; i++) {
            long key = i + j * N_ITEMS;
        pthread_mutex_lock(mutex);
            long got = map->find(key)->second;
        pthread_mutex_unlock(mutex);
        pthread_mutex_lock(mutex);
            map->insert(make_pair(key, tid));
        pthread_mutex_unlock(mutex);
        }
    }

    return NULL;
}

int main() {
    dict<long, long> d;
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);
    pthread_t threads[N_THREADS];
    for (int i = 0; i < N_THREADS; i++) {
        pthread_create(&threads[i], NULL,  worker, new worker_arg(&d, &mutex, i));
    }
    for (int i = 0; i < N_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}

