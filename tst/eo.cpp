#include "eo.h"

#include <assert.h>
#include <pthread.h>

#ifndef REPS
#define REPS 1000
#endif

void test_eobasic() {
    eo<int,1> one;
    optional<int> get;
    int sum1 = 0;
    int sum2 = 0;
    one.put(-1);
    get = one.get();
    assert(*get == -1);
    for (int i = 0; i < 100; i++) {
        one.put(i);
        get = one.get();
        assert(*get == i);
    }
    for (int i = 0; i < 100; i++) {
        one.put(i);
        sum1 += i;
    }
    for (int i = 0; i < 100; i++) {
        get = one.get();
        sum2 += *get;
    }
    assert(sum2 == sum1);
    one.close(0);
    get = one.get();
    assert(!get);
}
const int max_produce = 1000;
const int num_producers = 2;
const int num_consumers = 2;
// TODO parameterize producers
void* odds(void* arg) {
    eo<int,num_producers>* one = (eo<int,num_producers>*) arg;
    int* sum = (int*) malloc(sizeof(int));
    *sum = 0;
    for (int i = 1; i < max_produce; i+= num_producers) {
        one->put(i);
        *sum += i;
    }
    return sum;
}
void* evens(void* arg) {
    eo<int,num_producers>* one = (eo<int,num_producers>*) arg;
    int* sum = (int*) malloc(sizeof(int));
    *sum = 0;
    for (int i = 0; i < max_produce; i+= num_producers) {
        one->put(i);
        *sum += i;
    }
    return sum;
}
void* consume(void* arg) {
    eo<int,num_producers>* one = (eo<int,num_producers>*) arg;
    int* sum = (int*) malloc(sizeof(int));
    *sum = 0;
    optional<int> get;
    while ((get = one->get())) {
        *sum += *get;
    }
    return sum;
}
void test_eopara() {
    for (unsigned int i = 0; i < REPS; i++) {
        printf("%04X / %04X", i, REPS);
        for (size_t i = 0; i < 11; i++) {
            putchar('\b');
        }
        eo<int,num_producers> one;
        pthread_t producers[num_producers];
        pthread_t consumers[2];
        pthread_create(&producers[0], NULL, odds, &one);
        pthread_create(&producers[1], NULL, evens, &one);
        for (int i = 0; i < num_consumers; i++) {
            pthread_create(&consumers[i], NULL, consume, &one);
        }
        void* retval1;
        void* retval2;
        pthread_join(producers[0], &retval1);
        pthread_join(producers[1], &retval2);
        one.close(0);
        one.close(1);
        int* reduced1 = (int*) retval1;
        int* reduced2 = (int*) retval2;
        int sumProduced = *reduced1 + *reduced2;
        int sumConsumed = 0;
        for (int i = 0; i < num_consumers; i++) {
            void* retval3;
            pthread_join(consumers[i], &retval3);
            int* reduced3 = (int*) retval3;
            sumConsumed += *reduced3;
        }
        assert(sumConsumed == sumProduced);
    }
    for (size_t i = 0; i < 11; i++) {
        putchar(' ');
    }
    for (size_t i = 0; i < 11; i++) {
        putchar('\b');
    }
}
int main() {
    test_eobasic();
    test_eopara();
    return 0;
}
