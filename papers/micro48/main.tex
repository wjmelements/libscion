%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the template for submission to MICRO 2015
% The cls file is a modified from  'sig-alternate.cls'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{sig-alternate}

\newcommand{\ignore}[1]{}
\usepackage{fancyhdr}
\usepackage[normalem]{ulem}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{censor}
\usepackage{listings}
\usepackage{pgfplots}


\newcommand{\microsubmissionnumber}{262}

\fancypagestyle{firstpage}{
  \fancyhf{}
\setlength{\headheight}{50pt}
\renewcommand{\headrulewidth}{0pt}
  \fancyhead[C]{\normalsize{MICRO 2015 Submission
      \textbf{\#\microsubmissionnumber} -- Confidential Draft -- Do NOT Distribute!!}} 
  \pagenumbering{arabic}
}  

\title{A Wait-free Bucketing Multi-level Hash Table}

\begin{document}
\maketitle
\thispagestyle{firstpage}
\pagestyle{plain}

\begin{abstract}

This paper introduces a wait-free and linearizable dictionary that applies novel generation-counter and extensible bucketing strategies to a multi-level hash table.
The generation-counter reduces indirection while preserving correctness.
The key idea is to group puts into generations that occur between removes.
Concurrent removes are idempotent; they modify the least-significant bit of the generation-counter so that subsequent puts can establish the next generation.
Bucketing allows hash collision tolerance in a level of the hash table.
By implementing the bucket as an indexed array of pointers, the bucket can be safely and concurrently expanded.
A full bucket is resized into the next level of the hash table.
This avoids the problematic global resize that would otherwise delay concurrent progress.
\par
Generation-counters had twice the speed of indirection under high contention and provided a $25\%$ speedup under low contention.
The introduced structures were competitive with other lock-free solutions and were $300\%$ faster with 16 threads than locking approaches.
Generation counters and hazard pointers demonstrated the best speedup from concurrency of all the measured dictionaries at $475\%$ and $485\%$ respectively when going from 1 to 16 threads.
\end{abstract}

\section{Introduction}
A recent trend in modern CPUs as they approach physical speed limitations is towards more concurrency \cite{dally}.
However, collaborative parallelism is difficult to achieve in typical applications.
Following Amdahl's law \cite{amdahl}, performance gains from parallelism are limited by sequential portions of execution.
Synchronizing to protect concurrent memory accesses diminishes the speedup from parallelism because blocked threads cannot make progress.
Furthermore, yielding to other threads forfeits cache benefits.
\par
A dictionary, sometimes called a map, is a common abstraction that maps a subset of a key-space to a value-space.
Thread-safe dictionaries are useful for storing the state of application servers, artificial intelligence, and operating systems, and for objects in concurrent object-oriented programming runtimes.
In these situations, a more concurrent dictionary can replace a sequential bottleneck present in the locking approach.
\par
Desirable properties of concurrent objects include linearizability and wait-freedom.
Linearizability is a correctness condition where operations appear to atomically modify the state of the object at some point during their execution \cite{linearizability}.
Linearizability is a stronger consistency guarantee than sequential consistency, which only evaluates the consistency of the individual sequential executions. 
Wait-freedom is a progress guarantee that operations will complete in a bounded number of their own steps, regardless of the interleavings and relative speeds of other threads \cite{herlihy88}\cite{herlihy91}.
A weaker guarantee, lock-freedom, ensures that in any sufficiently long execution at least one operation has made progress.
All wait-free objects are lock-free.
Wait-free objects are desirable because they scale in the number of processors, whereas lock-free objects suffer under contention.
\par
The dictionary this paper introduces is linearizable and wait-free for most practical key types, including integers, pointers, and strings.
It maintains a collection of nodes linked in a multi-level hash table.
A dictionary node is a permanent home for a key even after the key is removed from the dictionary.
This is both a performance feature and an overhead.
Hazard pointer declarations can be avoided and branching is limited.
A new node does not have to be allocated to re-insert the key.
However, a removed key will continue to consume an amount of space and slow colliding accesses.
Additionally, the structure cannot shrink once expanded.
\par
The dictionary can be augmented with generation-counters, a novel technique that reduces indirection by grouping puts into generations.
The generation-counter reserves its least-significant bit to mark if the corresponding key is removed.
{\tt remove} atomically sets that bit using atomic {\tt or} and {\tt put} advances it with a compare and swap (CAS).
Only puts that observe the removal mark need to update the generation-counter.
While generation-counters can be unstable due to integer overflow, they are reliable for practical systems.
Total correctness is achieved with hazard pointer indirection, also described in this paper.
\par
The structures described in this paper make use of compare and swap (CAS), an atomic instruction provided by many commodity architectures.
A memory location is updated if it is equal to an expected value \cite{IBM}\cite{michael2002}, and flags are set to indicate success.
On failure, it reads the unexpected value into the register containing the expected value.
Load-link/store-conditional (LL/SC), an equivalent instruction pair, can also be used for this structure.
Store-conditional updates a memory location only if it has not changed since load-link \cite{michael2002}.
Load-link/store-conditional is often more difficult for an architecture to provide due to context-switching overheads.
\par
The paper is organized as follows.
Section \ref{prior work} describes contributions leading to this result.
Section \ref{main result} describes the main result of this paper, the wait-free hash table.
Section \ref{benchmarking} benchmarks the structure against previous structures and alternative approaches, including locking.

\section{Prior Work}
\label{prior work}
Hazard pointers are a concurrent memory management scheme developed by Maged Michael at IBM \cite{hazard}.
The dictionary this paper describes can use hazard pointers instead of generation-counters for value-mapping.
Hazard pointers work by announcing intended dereferences and retiring dynamic memory before freeing it.
Memory managed by hazard pointers transitions through several states.
A dynamic node is $reachable$ if other threads can derive it from an object's base pointers.
While a node is reachable, threads can begin the process of trying to access its memory.
Before dereferencing a managed node, a thread must declare its intent to dereference the pointer and then rederive it.
A node is $removed$ when it is no longer reachable.
The thread that successfully performed the removal is tasked with retiring the node.
A $retired$ node is placed in a thread-local set-like structure.
A thread can free a retired node if no other thread has declared that it intends use the node's memory.
When a thread has retired sufficiently many nodes, it can guarantee that looping through its retired nodes will result in a linear number of successful frees.
An unbounded amount of retries in rederiving a pointer would make applications of this procedure lock-free instead of wait-free.
If an algorithm can bound the number of retries then this approach is wait-free.
\par
Maged Michael also developed a lock-free hash table \cite{michael2002}.
The structure uses lock-free sorted linked-lists to bucket hash collisions.
The vanilla structure does not support resize in any form, however, and degrades to linear performance if it underestimates its final size.
\par
Shalev and Shavit use a lock-free sorted linked-list to maintain the entire mapping and maintain buckets as pointers to meta-nodes in the list \cite{shalev}.
\par
Gao et al. developed a lock-free hash table with open addressing that can globally grow or shrink by switching into a resizing state \cite{gao}.
\par
Feldman, LaBorde, and Dechev claim a wait-free hash table \cite{feldman}.
Their hash table, like the one this paper presents, uses multi-level arrays to avoid a global resize.
Their structure is not completely wait-free, however.
Implementing put with hazard pointers as they describe would render {\tt get} unbounded lock-free as it can continuously fail to rederive a node pointer.
Expanding under contention as they suggest does not solve this problem.
Further, they lack support for hash collisions and they have severe restrictions on the permitted key spaces; for example, they cannot support strings.
\section{The Dictionary}
\label{main result}
This section describes the main result of this paper, the wait-free dictionary.
Subsection \ref{node structure} describes the generation-counter approach as well as the hazard pointer indirection approach it replaces.
Subsection \ref{table structure} describes the table walking and local resize through rehashing.

\subsection{Node Structure}
\label{node structure}
\begin{figure}
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{code/indnode}
    \caption{Node Structure: Hazard Pointer}
    \label{indnode_s}
\end{figure}
\begin{figure}
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{code/gennode}
    \caption{Node Structure: Generation Counter}
    \label{gennode_s}
\end{figure}
A node that is successfully inserted into the table is a permanent home for a key-value pairing.
Node structure code is shown in Figures \ref{indnode_s} and \ref{gennode_s}.
Nodes contain the key they maintain and a mechanism for value mapping.
Because the hash of the key is required in rehashing, described in Section \ref{table structure}, it is optionally saved in the node as well.
Non-rigorous benchmarking demonstrated a speedup from storing the hash of a key instead of recalculating it.
\par
Both the hazard pointer and the generation-counter approaches work by grouping puts into generations.
For the hazard pointer, a generation is a pointer to a location at which multiple puts can succeed.
Removes exchange this pointer with {\tt NULL} while puts that read {\tt NULL} attempt to establish the next generation.
Removal is therefore idempotent; removals on a removed state will receive {\tt NULL} from their exchange.
A significant feature of this mechanism is that generations only end during a remove.
If during a put the pointer changes before it is rederived, then a remove has occurred concurrently and the put is sequenced before that remove.
Similarly, if a pointer changes during a get, then at some point during the execution of get, the value was removed.
Therefore, neither operation that dereferences the memory needs to rederive it more than once.
\par
The generation-counter works similarly except without the indirection and dynamic allocation overheads of hazard pointers.
Odd generation numbers indicate the removed state while even generation numbers indicate the valid state.
Removal can therefore atomically mark the end of a generation by setting the least significant bit.
Similar to before, removal is idempotent.
Get reads the generation-counter, the value, and then the generation-counter again.
If the generation-counter has changed or is odd, then at some point during the execution of get, the value was in a removed state.
Otherwise, get can successfully return the value.
Put reads the generation-counter, updates the value, and, if the generation read was odd, CAS's in the next generation.
The total ordering of the puts is determined by the order in which they update the value.
If the {\tt or} of a remove occurs before the CAS of a put, then that remove occurred before the put.
If the {\tt or} occurs after the CAS, then that remove occurred after the put.
A get that reads an odd generation number occurred after some removal and before any concurrent put.
A get that reads a changed generation number occurred after a concurrent remove.
A get that reads no change in an even generation number occurred some time after the put that stored the value in that location.
All operations appear to occur atomically between their invocation and their response.
\par
The correctness of generation-counters relies on ABA safety.
An ABA problem exists if reading the same value incorrectly implies no change \cite{IBM}\cite{hazard}.
Unfortunately, it is possible for the generation number to be equal because it has overflowed and returned to the previous state.
This situation is highly improbable for 64-bit counters because it requires at minimum $2^{63}$ removals and puts on the same key during the preemption of a thread, and for the generation-counter to have returned to the same value that was read before.
However, this possibility is sufficient to undermine rigorous correctness because it can cause a put to appear to happen before and after a remove.
Note that using LL/SC instead would correct this problem, because store-conditional would fail.
This paper will assume that this algorithm will suffer from the ABA problem 0 times before the death of computing.
\par
The performance of these two techniques is compared in Section \ref{benchmarking}.
The generation-counter generally outperforms the hazard pointer due to the reduced caching overheads of indirection.
\par
If removal is not required, then the node can be further optimized to not use a generation-counter.
Puts would only need to update the value, and gets only need to read the value.

\subsection{Table Structure}
\label{table structure}
\begin{figure}
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{code/table}
    \caption{Table Data Structure}
    \label{table_s}
\end{figure}
\begin{figure}
    \includegraphics{figures/table.eps}
    \caption{Table Structure, $B=2$, $C=3$, $22$ Keys}
    \label{table}
\end{figure}
Table structure pseudocode is given in Figure \ref{table_s}.
The multi-level hash table considers $B$ bits at each level to index into one of the $N=2^B$ rows.
A row is a fixed-size array of $C$ pointers, initially {\tt NULL}, and a pointer to the next level.
Rows are filled left-to-right by put operations and then expanded into the next level.
All table pointers are set once with a CAS operation that expects {\tt NULL}.
The structure is shown in Figure \ref{table}.
\par
\begin{figure}
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{code/find}
    \caption{Find}
    \label{find_cpp}
\end{figure}
The find operation returns a pointer to a node.
Find pseudocode is shown in Figure \ref{find_cpp}.
First, a hash is calculated from the hash function.
The table is walked using the next $B$ least significant bits at each level.
Once the final level is reached, find iterates over the row, searching for the node with the correct key.
If the node was found in this row, then at the end the table-walking no node for this key had yet been put and find can return.
Because rows are filled sequentially, find can also terminate upon reading a {\tt NULL} entry.
{\tt get} and {\tt remove} can be implemented by performing a find and then the corresponding subroutine from Section \ref{node structure} on the returned node.
\par
\begin{figure}
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{code/put}
    \caption{Put}
    \label{put_cpp}
\end{figure}
The put operation unconditionally maps a value to the key.
Pseudocode for put is found in Figure \ref{put_cpp}.
The table-walking is similar to find.
If put comes across a {\tt NULL} entry, it CAS's in a new node expecting {\tt NULL}, returning on success.
If put discovers a node with the key, it performs a put on that node and returns.
If put finishes iterating over a row, it must expand that row into the next level.
It does this by rehashing the node pointers into the next table using a divisor tracked during the table walking.
Hash values for the keys in that row are either stored in the nodes or recalculated.
\par
The linearization point for a put that successfully CAS's in a node is the succeeding CAS.
The linearization point for a find that finds a {\tt NULL} entry is the {\tt NULL} entry read.
The linearization point for a find that finds neither a {\tt NULL} entry nor a matching entry in its row is the {\tt NULL} table-pointer read during traversal.
\par
An estimate of the table size can be maintained by incrementing an integer upon successfully CAS'ing either a pointer or a generation number during a put.
The generation-counter version of remove would need to perform {\tt fetch-or} instead of {\tt or} in order to know if it needs to update the dictionary size counter.
The hazard pointer version of remove would atomically decrement the counter only if it retired a pointer.

\subsection{Wait-freedom}
For key-spaces whose hash functions have a bounded number of complete hash collisions, this structure is bounded wait-free, because the worst-case traversal times for both find and put are bounded.
For key-spaces such as the strings, there exist hash functions (such as identity) that are bijections to the variable-length bitstrings.
The structure could handle variable-length hashes with an additional $2^B-1$ pointers corresponding to the possible hash terminations at each level.
This structure would be wait-free for any key-space with a bijection to the variable-length bitstrings, as any traversal would be bounded linear in the length of its hash.

\section{Benchmarking}
\label{benchmarking}
All benchmarking was performed on TACC Stampede Compute Nodes.
A Compute Node has 2 processor sockets each with an Intel Xeon E5-2680 processor.
Each processor clocks at 2.7 GHz and has 20 MB of cache, 8 cores, and 16 threads of execution.
The Compute Node also has 32 GB of memory clocked at 1600 MT/s and a coprocessor which was not used.
\par
All benchmarks were compiled with the same flags, with the exception of the Michael structure, which required {\tt -fexceptions}.
Care was taken to ensure that gets and other {\tt const} operations were not optimized out.
A Michael map implementation was fetched from libcds on github.
The Feldman et al. map code was retrieved from their website.
It had a clever optimization to reuse allocations, but it suffered from probabilistic memory errors.
All of the structures were implemented in C++.
Every benchmark was run $5$ times for every number of threads measured.
\par
\begin{figure}
    \begin{tikzpicture}
        \begin{axis}[
            legend style={
                at={(1.00,1.00)},
                anchor=north east,
            },
            legend cell align=left,
            xlabel=Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    dict={mark=diamond, draw=red},
                    indict={mark=triangle, green},
                    mich={mark=o, draw=blue},
                    feld={mark=square, cyan}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{data/pg};
            \legend{Generation,HazardPointer,MichaelLF,FeldmanLF}
        \end{axis}
    \end{tikzpicture}
    \centering
    \caption{Non-blocking Performance: $52\%$ Put, $48\%$ Get}
    \label{pg}
\end{figure}
Figure \ref{pg} shows the performance of the structures under a workload that does not remove.
The total work was 17 puts and 16 gets for each of $40$ million keys.
The work was divided evenly among the threads, which alternated between puts and gets.
The shape of the graph if there was $100\%$ parallel portion would be the reciprocal function, $\frac{1}{x}$.
While every lock-free approach benefited from multiple threads, some leveled out sooner than others.
The structure of Feldman, LaBorde, and Dechev slowed with additional threads after the 6th.
The fastest lock-free structure was Maged Michael's, which was advantaged here in knowing the number of keys during initialization.
The generation-counter approach (Generation) outperformed its indirection counterpart (HazardPointer) with a consistent $30\%$ speedup.
Bucketing, performed by the hazard pointer and generation-counter approaches introduced in this paper as well as in the Michael structure, performed better in the presence of multiple threads, probably due to cache coherence policies.
The structures introduced in this paper had the worst performance for a single thread but showed significant speedup ratios with additional threads.
Note that this benchmark did not use the described removal-free optimization.
\par
\begin{figure}
    \begin{tikzpicture}
        \begin{axis}[
            legend style={
                at={(1.00,0.50)},
                anchor=east,
            },
            legend cell align=left,
            xlabel=Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    mutex={mark=triangle, magenta},
                    rw={mark=o, cyan}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{data/rw};
            \legend{pthread\_mutex,pthread\_rwlock}
        \end{axis}
    \end{tikzpicture}
    \centering
    \caption{Comparing Locking Approaches}
    \label{rw}
\end{figure}
The performance of locking approaches on the same benchmark is shown in \ref{rw}.
Mutual exclusion on the STL {\tt unordered\_map}, labeled as LockSTL in the graph, performed better than any other approach at 1 thread, but suffered under concurrency, slowing $330\%$ for 2 threads and $730\%$ for 4 threads.
Mutual exclusion performed its worst at 4 threads.
That peak seems specific to that processor because it did not reproduce on an i7 or on AMD FX.
The performance of read-write locking, which attempts to allow concurrent reading, was approximately 4 times worse than mutual exclusion.
This can be attributed to the alternating read/write pattern, which thrashes read-write locks.
\par
\begin{figure}
    \begin{tikzpicture}
        \begin{axis}[
            xbar,
            xlabel=Memory (MB),
            symbolic y coords={STL,FeldmanLF,MichaelLF,HazardPointer,Generation},
            ytick style={draw=none},
            axis on top,
            y tick label style={anchor=west,color=black,xshift=\pgfkeysvalueof{/pgfplots/major tick length}/2},
            xmin=0,
            xmax=57,
            nodes near coords,
            nodes near coords align={horizontal}
        ]
            \addplot coordinates {
                (29.061120,Generation)
                (35.386560,HazardPointer)
                (14.942208,MichaelLF)
                (47.984640,FeldmanLF)
                (17.022976,STL)
            };
        \end{axis}
    \end{tikzpicture}
    \centering
    \caption{Memory Consumption: $400,000$ Keys}
    \label{mem}
\end{figure}
Figure \ref{mem} shows the memory consumption of $400,000$ keys in each of the structures.
Memory consumption was measured with {\tt mallinfo} before the dictionaries were deallocated.
The dictionary of Feldman, LaBorde, and Dechev was expected to consume less memory than the structure introduced in this paper, because of its storing next-pointers in the same location as data pointers. 
However, the lack of bucketing caused avoidable expansions leading to additional overhead.
The Michael dictionary had an advantage in memory consumption in that it knew ahead of time how many keys were going to be placed.
It does not resize and does not suffer from speculative memory overheads.
\par
\begin{figure}
    \begin{tikzpicture}
        \begin{axis}[
            legend style={
                at={(0.51,1.00)},
                anchor=north,
            },
            legend cell align=left,
            legend style={font=\scriptsize},
            xlabel=Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    dict={mark=diamond, draw=red},
                    indict={mark=triangle, green},
                    mich={mark=o, draw=blue},
                    feld={mark=square, cyan}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{data/sing};
            \legend{Generation,HazardPointer,MichaelLF,FeldmanLF}
        \end{axis}
    \end{tikzpicture}
    \centering
    \caption{Single Key: $33\%$ Put, $33\%$ Get, $33\%$ Remove}
    \label{sing}
\end{figure}
Another benchmark considered the performance at highest contention, where all operations are on the same key.
The total work was $320$ million rotations of put, get, and remove, divided evenly among a number of threads.
The results are shown in Figure \ref{sing}.
For more than one thread, both the hazard pointer and generation-counter approaches introduced in this paper outperformed the alternatives.
Furthermore, the generation-counter approach completed in half the time of the hazard pointer approach.
This is probably due to branch predictability and the wait-free nature of the operations.
Because a node is the permanent home of a key, many puts did not require an allocation.
After the second thread, additional threads gave neither a speedup nor a slowdown in any approach.
This can be attributed to cache consistency policies, which ensure the sequential consistency of CAS through a mechanism similar to mutual exclusion.
Speedups from concurrency require threads to independently succeed in applying their operations on different cache lines.
\par
\begin{figure}
    \begin{tikzpicture}
        \begin{axis}[
            legend style={
                at={(0.51,1.00)},
                anchor=north,
            },
            legend cell align=left,
            legend style={font=\scriptsize},
            xlabel=Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    dict={mark=diamond, draw=red},
                    indict={mark=triangle, green},
                    mich={mark=o, draw=blue},
                    feld={mark=square, cyan}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{data/rm};
            \legend{Generation,HazardPointer,MichaelLF,FeldmanLF}
        \end{axis}
    \end{tikzpicture}
    \centering
    \caption{Non-blocking Performance: $91\%$ Get, $6\%$ Put, $3\%$ Remove}
    \label{rm}
\end{figure}
Another benchmark, shown in Figure \ref{rm},  measured a workload with $91\%$ gets, $6\%$ puts, and $3\%$ removes.
The total work was spread over $40$ million keys and divided among a number of threads.
Due to the low number of puts and removes, this was a low-contention benchmark.
The structure of Feldman et al. completed with 16 threads in one third of the time it took with 1 thread.
The Michael map only achieved a $150\%$ speedup between 1 and 16 threads, but outperformed all maps for any number of threads.
Once again, it had the advantage of knowing the number of keys that were being put.
The generation-counter approach defeated its indirection counterpart by approximately $25\%$ at every number of threads.
The speedup for generation-counters moving from 1 thread to 16 threads was $475\%$, while the speedup for hazard pointers was $485\%$.
Again, the introduced dictionaries demonstrated the best concurrency speedup ratios.
However, on this benchmark they did not overtake Feldman et al.
Ideally, under lower contention, there should be even higher speedup ratios, since the operations are mostly independent.

\section{Conclusion}
This paper introduced two techniques enabling wait-free bucketing and wait-free updating with reduced indirection.
In combination with previous approaches this gives a wait-free extensible hash table without any CAS retries.
The structure is highly configurable for individual platforms and workloads, and becomes even faster if remove is not required.
It expects CAS or LL/SC, present on most commodity architectures.
The ideas in this paper are steps toward more useful asynchronous concurrency using shared memory.

\bibliographystyle{ieeetr}
\bibliography{ref}

\end{document}
