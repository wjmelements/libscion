#ifndef SC_BUILTINS
#define SC_BUILTINS
#include "stream.h"

void* STDOUT(void* out);
void* STDERR(void* err);
void* STDIN(void* in);

struct argv_args {
    stream<char*>* arg_stream; // stream of strings
    int argc;
    char** argv; // array of strings
};
void* ARGV(void* args); // stream, argc, argv

struct read_file_args {
    char* path;
    stream<char*>* lines;
};
void* read_file(void* args);

struct read_dir_args {
    char* path;
    stream<char*>* filenames;
};
void* read_dir(void* args);
#endif
