#ifndef scion_spec
#include "optional.h"
#include <stdlib.h>
template <typename T>
#endif
class spsc {
private:
    struct node {
        #ifdef scion_seq
        uint64_t seq;
        #endif
        node* next;
        T data;
    } *first, *last;
    #ifdef scion_seq
    atomic<uint64_t>* seq;
    #endif
public:
    spsc() {
        first = last = (node*) malloc(sizeof(node));
        first->next = NULL;
    }
    ~spsc() {
        while (first) {
            node* to_free = first;
            first = first->next;
            free(to_free);
        }
    }
    #ifdef scion_seq
    inline void setSeq(atomic<uint64_t>* source) {
        seq = source;
    }
    optional<uint64_t> peekSeq() {
        if (first == last) {
            return false;
        }
        return first->seq;
    }
    #endif
    void enq(T item) {
        #ifdef scion_seq
        last->seq = seq->fetch_add(1);
        #endif
        last->data = item;
        node* next = (node*) malloc(sizeof(node));
        next->next = NULL;
        last->next = next;
        last = next;
    }
    optional<T> deq() {
        optional<T> ret;
        if (last == first) {
            ret.valid = false;
            return ret;
        }
        ret.valid = true;
        ret.value = first->data;
        node* to_free = first;
        first = first->next;
        free(to_free);
        return ret;
    }
};
