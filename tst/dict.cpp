#include "dict.h"

#include <assert.h>
#include <pthread.h>

#ifndef NTHREADS
#define NTHREADS 16
#endif

void test_singleport() {
    dict<int, int,1,1> two;
    /*
    assert(two.cbegin() == two.cend());
    two.put(1,1);
    assert(two.cbegin() != two.cend());
    auto it = two.cbegin();
    ++two.cbegin();
    assert(it == two.cend());
    */
    int i;
    for (i = 0; i < 100; i++ ) {
        two.put(i, i + 1);
    }
    i = 0;
    while (i < 100) {
        optional<int> got = two.get(i);
        assert(got);
        assert(*got == i + 1);
        i = *got;
    }
}
void test_ops() {
    dict<int, int,10,5> one;
    //assert(one.cbegin() == one.cend());
    assert(!one.get(100));
    auto loc = one.find(100);
    assert(!loc);
    assert(!one.get(100));
    one.put(100, 200);
    //assert(one.cbegin() != one.cend());
    loc = one.find(100);
    optional<int> got = one.get(100);
    assert(got);
    assert(*got == 200);
    //auto it = one.cbegin();
    loc = one.find(100);
    assert(loc);
    assert(loc->first == 100);
    assert(loc->second.load() == 200);
    //it++;
    //assert(it == one.cend());
    int expected = 100;
    optional<bool> found;
    /*
    found = one.compare_exchange_strong(100, expected, 150);
    assert(found);
    assert(!*found);
    */
    got = one.get(100);
    assert(got);
    assert(*got == 200);
    /*
    expected = 200;
    found = one.compare_exchange_strong(100, expected, 150);
    assert(found);
    assert(*found);
    got = one.get(100);
    assert(got);
    assert(*got == 150);
    */
    one.remove(100);
    got = one.get(100);
    assert(!got);
    got = loc->get();
    assert(!got);
    loc->put(151);
    got = loc->get();
    assert(*got == 151);
    /*
    found = one.compare_exchange_strong(101, expected, 150);
    assert(!found);
    */
    /* TODO fix iteration
    for (int i = 101; i <= 200; i++) {
        assert(!one.put(i,i+1));
        assert(one.get(i));
        assert(*one.get(i) == i+1);
    }
    size_t count = 0;
    for (auto it = one.cbegin(); it != one.cend(); it++) {
        assert(one.get(it->first) == it->second);
        count++;
    }
    assert(count == 101);
    count = 0;
    for (auto it = one.begin(); it != one.end(); it++) {
        assert(one.get(it->first) == it->second);
        count++;
    }
    assert(count == 101);
    dict<int,int,1,1> all_collide;
    for (int i = 0; i < 500; i++) {
        assert(!all_collide.put(i,i+1));
        for (int j = 0; j <= i; j++) {
            assert(all_collide.get(j));
            assert(all_collide.get(j) == j+1);
        }
    }
    dict<int,int,1,2000> all_inline;
    for (int i = 0; i < 100; i++) {
        assert(!all_inline.put(i,i+1));
        for (int j = 0; j <= i; j++) {
            assert(all_inline.get(j));
            assert(all_inline.get(j) == j+1);
        }
    }
    */
}
void test_large() {
    dict<long, long,2,1> two;
    for (long i = 0; i < 800000; i++) {
        two.put(i,i);
        assert(*two.get(i) == i);
    }
    for (long i = 0; i < 800000; i++) {
        assert(*two.get(i) == i);
    }
}
void *do_nothing(void*a){return NULL;}
void *put_self(void* varg) {
    dict<pthread_t,void*> *d = (dict<pthread_t,void*>*) varg;
    d->put(pthread_self(), d);
    assert(d->get(pthread_self()));
    assert(d->get(pthread_self()) == d);
    return NULL;
}
void test64() {
    dict<pthread_t,void*> d;
    pthread_t threads[NTHREADS];
    for (int i = 0; i < NTHREADS; i++) {
        pthread_create(&threads[i], NULL, put_self, &d);
    }
    d.put(pthread_self(), &d);
    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(threads[i],NULL);
    }
    assert(d.get(pthread_self()) == &d);
    for (int i = 0; i < NTHREADS; i++) {
        assert(d.get(threads[i]).valid);
        assert(d.get(threads[i]) == &d);
    }
}
struct test_para_arg {
    dict<off_t, off_t>* d;
    size_t id;
};
template<off_t offset> void* test_para_job(void* arg) {
    struct test_para_arg* tparg = (test_para_arg*) arg;
    dict<off_t, off_t>* d = tparg->d;
    off_t id = tparg->id;
    free(arg);
    

    d->put(id, id + offset);
    optional<off_t> got = d->get(id);
    assert(got);
    assert(*got == id - offset || *got == id + offset);
    return NULL;
}
void test_para() {
    dict<off_t,off_t> d;
    pthread_t threads[NTHREADS];
    off_t num_keys = NTHREADS / 2;
    for (off_t id = 0; id < num_keys; id++) {
        test_para_arg* arg;

        arg = (test_para_arg*) malloc(sizeof(*arg));
        arg->d = &d;
        arg->id = id;
        pthread_create(&threads[id], NULL, test_para_job<1>, arg);

        arg = (test_para_arg*) malloc(sizeof(*arg));
        arg->d = &d;
        arg->id = id;
        pthread_create(&threads[id + num_keys], NULL, test_para_job<-1>, arg);
    }
    for (off_t id = 0; id < num_keys; id++) {
        optional<off_t> got = d.get(id);
        if (got) {
            assert(*got == id - 1 || *got == id + 1);
        }
        pthread_join(threads[id], NULL);
        pthread_join(threads[id + num_keys], NULL);
        got = d.get(id);
        assert(got);
        assert(*got == id - 1 || *got == id + 1);
    }
}
int main() {
    test_singleport();
    test_ops();
    test_large();
    for (int i = 0; i < 100; i++) {
        test64();
    }
    for (size_t i = 0; i < 100; i++) {
        test_para();
    }
    return 0;
}
