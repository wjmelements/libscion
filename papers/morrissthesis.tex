\documentclass{article}
\usepackage{listings}
\usepackage{pgfplots}
\pgfplotsset{compat=1.12}
\title{A Concurrent Dictionary and Wait-Free Queue from Compare and Swap}
\author{William Morriss}

\newtheorem{theorem}{Theorem}[section]

\begin{document}
\maketitle
\begin{abstract}
This paper describes algorithms for a lock-free dictionary and a wait-free queue.
The queue improves on a previous result by Kogan and Petrank by not requiring Stop-The-World Garbage Collection.
The lock-free dictionary is structured as a multi-level hash table to avoid global resize.
It can be made wait-free for most practical key types with trivial modifications.
The dictionary supports sequentially consistent compare and swap operations in addition to exchange and put, but does not support removal.
It was over $3$ times faster than a previous concurrent multi-level hash table result.
The data structures in this paper expect a compare and swap primitive, found in many commodity architectures including x86, x64, and Itanium.
\end{abstract}
\setlength{\parskip}{1em}
\section{Introduction}
    \subsection{Purpose}
        The recent trend in modern CPUs as they approach physical speed limitations is towards more concurrency \cite{dally}.
        Following Amdahl's law \cite{amdahl}, the sequential portion of a computation limits its attainable speedup from adding more cores.
        Mutual exclusion, an approach that marks ``critical sections'' that only one thread may enter at a time, reduces concurrency by increasing this sequential portion.
        It is critical for continued improvements in the speed of software to find alternative solutions that scale with the number of cores. 
        \par
        A dictionary is a common abstraction that maps a subset of a key-space to a value-space.
        Concurrent dictionaries are useful for storing the state of application servers, artificial intelligence, and operating systems, and for objects in concurrent object-oriented programming runtimes.
        In these situations they solve a sequential bottleneck in the locking approach.
        This paper presents a lock-free dictionary that outperforms previous approaches for large numbers of threads.
        The dictionary is a multi-level hash table that is indexed using different bits of the hash at each level.
        Hash collisions are resolved with a fixed-size bucketing approach and when the bucket is full, it is expanded into the next level.
        \par
        This paper also considers queues and memory management.
        A first-in first-out (FIFO) queue accepts elements and returns them in the order that they are given.
        A parallel queue could be used for work queues and free-lists.
        A previous result provided a wait-free queue but did not detail its required memory management.
        While Stop-The-World garbage collection is sufficient for memory management for many applications, it is not sufficient for complete wait-freedom,
        because the system must suspend all progress to free up the heap.
        This paper describes an application of hazard pointers, a concurrent memory management technique, to that queue
        and benchmarks the performance against garbage collection.
    \subsection{Definitions}
        In this paper, a thread is a software abstraction of an I/O automaton.
        Threads communicate with shared memory capable of sequentially consistent fetch and add, exchange, and compare and swap.
        Threads execute at arbitrary speeds, can interleave arbitrarily, and can be preempted indefinitely.
        An asynchronous structure, or object, is implemented using sets of shared memory locations and is defined in terms of its states and its operations.
        Objects should be linearizeable as defined by Herlihy and Wing \cite{linearizability}:
        state changes appear to happen atomically at some time between an operation's invocation and its response.
        Wait-freedom and lock-freedom are progress guarantees on an object.
        An object is lock-free if and only if some operation on the object will make progress for any sufficiently long execution.
        An object is wait-free if and only if every step in the execution of every operation makes progress.
        All wait-free objects are lock-free.
        Lock-freedom guarantees object-wide progress while wait-freedom guarantees the progress of every thread.
        In some literature, lock-free is called wait-free and wait-free is instead distinguished as bounded wait-free,
        because each operation will complete in a bounded number of its own steps.
        Objects are useful because they compartmentalize concurrent memory accesses and therefore assist in the design of 
        lock-free or wait-free data structures and algorithms, and because they are self-contained.
        \par
        \begin{figure}
            \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{papers/code/cas}
            \caption{Compare and Swap (CAS)}
            \label{cas}
        \end{figure}
        Compare and swap (CAS) atomically sets a value if it was previously an expected value.
        Otherwise, it atomically reads the actual value.
        Pseudocode for this is shown in Figure \ref{cas}.
        CAS is useful for committing possibly concurrent modifications to shared memory.
        Usually the expected value is $null$ or some previously read value.
        CAS usually requires some form of cache line ownership and so cache line size should be considered when optimizing concurrent data structures.
        CAS is universal; it can solve consensus among any number of processors \cite{herlihy91}.
    \subsection{Outline}
        The rest of this paper is organized as follows:
        The second section presents related prior work.
        The third section describes the dictionary this paper introduces.
        The fourth section describes the application of hazard pointers to the queue of Kogan and Petrank.
\section{Prior Work}
    Jayanti and Petrovic produced a wait-free multiple-producer single-consumer queue in \cite{jayanti2005}.
    It was built on wait-free single-producer single-consumer queues and a binary tree.
    The performance is logarithmic in the number of threads.
    \par
    Kogan and Petrank developed a multiple-producer multiple-consumer wait-free queue in 2011 \cite{kogan2011}.
    Operations are grouped into semi-sequential phases, represented by a long integer.
    Each operation joins a phase by observing the phase of all other operations and joining the phase after those phases.
    A thread completing its operation loops over the operations and helps the incomplete ones in phases not after its own.
    The internal structure of the queue is a singly linked-list and an array of operation descriptions.
    In the {{\tt help\_finish\_enq}} and {{\tt help\_finish\_deq}} operations, the queue is made ready for another operation.
    In the {{\tt help\_enq}} and {{\tt help\_deq}} operations, a helping thread learns the existing structure of the queue and then performs a compare and swap to commit an appropriate action.
    If the queue is changed during this time then the thread restarts helping that operation, if it should still be helped.
    The number of retries is bounded because threads can only fail if another thread has made progress.
    If threads working on a particular operation continue to fail, then eventually all threads working on the queue will be helping that operation.
    Unfortunately, {{\tt help}} is linear in the number of threads.
    The given algorithm also has a reliance on stop-the-world garbage collection (STWGC) for memory management.
    \par
    Hazard pointers are a memory management scheme developed by Maged Michael at IBM \cite{hazard}.
    They work by transitioning dynamic memory through several states.
    A dynamic node is $reachable$ if other threads can derive it from an object's pointers.
    While a node is reachable, threads can begin the process of trying to access its memory.
    A node is $removed$ if it is no longer reachable.
    The thread that successfully performed the removal is tasked with retiring the node.
    A $retired$ node is placed in a thread-local set-like structure.
    When a thread has sufficiently many retired nodes, it can guarantee that checking its retired nodes will result in a successful free.
    A thread can free a retired node if no other thread has declared that it intends use the node's memory.
    Before dereferencing a node managed by hazard pointers, a thread must declare its intent to dereference and then rederive it.
    The unbounded amount of retries makes this procedure lock-free instead of wait-free.
    If an algorithm can bound the number of retries then it can make this approach wait-free.
    \par
    Maged Michael developed a lock-free hash table in 2002 \cite{michael2002} to implement a set.
    The set uses lock-free sorted linked-lists to bucket hash collisions.
    The structure does not support resize in any form, however.
    \par
    Feldman, LaBorde, and Dechev claim a wait-free hash table \cite{feldman}.
    Their hash table, like the one this paper presents, uses multi-level arrays to avoid a global resize.
    It further supports removal.
    However, it is not wait-free because removes and puts on the same key can starve a thread attempting to perform a put or a removal.
    Their algorithm would notice contention and expand repeatedly until the final level.
    After the final level there would be nowhere left to expand and the operation could continue to loop due to conflicting modifications.
    That interleaving is improbable but sufficient to reject their claimed wait-freedom property.
    Similarly, implementing their remove scheme with hazard pointers as they describe would render $get$ unbounded lock-free because of continuous interruption.
    Further, they lack of support for hash collisions and they have severe restrictions on the permitted key spaces.
\section{Lock-free Dictionary}
\subsection{Structure}
\begin{figure}[h]
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{papers/code/dictstruct}
    \caption{Dictionary Structures}
    \label{dictstruct}
\end{figure}
\begin{figure}[t]
    \includegraphics[width=\textwidth]{papers/images/tree.png}
    \caption{Dictionary Structure ($n=4$, $c=4$)}
    \label{dictcolor}
\end{figure}
The structure is a multi-level hash table.
Each level considers the next $b$ least-significant bits of the hash and contains $n = 2^b$ rows, each with $c$ node pointers and a pointer to the next level.
$c$ and $b$ are configurable constants.
A node contains a key, the hash of the key, and the current value mapped from that key.
In the final level, the pointer is not to another table with $n$ rows because there are no more bits with which to index.
Instead, the pointer is to an adjacent level with $1$ row containing $c$ node pointers and a pointer to another adjacent level.
Each node linked in the table is the permanent home for the value corresponding to its key.
Rows of a level are filled sequentially and then the next level is created containing all of the node pointers in the previous row.
This rehashing is similar to a local resize; a row is dynamically expanded into a table at the next level.
The next level is not larger but fewer keys are candidates to be found in it. 
Benchmarking a strategy that does not rehash a completed row into the next level demonstrated worse performance on multiple architectures.
\par
The structure is illustrated in Figure \ref{dictcolor}.
Each rectangle represents a pointer.
The colors here correspond to the keys of the nodes pointed to in the table, and white in the table represents a $null$ pointer.
The root level is the left-most table.
The drawn rows fill left-to-right.
\par
The total space held by the structure is linear in the number of keys it contains.
This is because tables are only created to accomodate items not held in the parent row, and because only one node can be committed per key.
\par
C++-like sample code is included in Figures \ref{dictstruct}, \ref{dictfind}, and \ref{dictput}.
It contains shorthand and for simplicity does not implement the last level.
The actual code can be retrieved from bitbucket.
\subsection{{{\tt find}}}
\begin{figure}[h!]
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{papers/code/dictfind}
    \caption{Dictionary {{\tt find}}}
    \label{dictfind}
\end{figure}
A thread performing a {{\tt find}} operation indexes into its row in the root table by using the least significant $b$ bits of its hash.
If the pointer to the next level is non-$null$, the thread continues this operation at the next level for its row with the next $b$ bits.
When the thread can no longer descend into the tree or reaches the final level, it iterates over the $c$ node pointers.
If it comes across a node with the desired key, it can return a pointer to the node's value.
If it reaches a $null$ pointer in its iteration or if it completes its iteration then it can return $null$, indicating the key was not found.
{{\tt find}} can implement {{\tt get}} by dereferencing the pointer it returns, but it can also provide an interface for atomic fetch-and-add and compare-and-swap on values in the dictionary,
because a committed node is the permanent host of a key.
\subsection{{{\tt put}}}
\begin{figure}[t!]
    \lstinputlisting[language=C++,frame=single,basicstyle=\scriptsize]{papers/code/dictput}
    \caption{Dictionary {{\tt put}}}
    \label{dictput}
\end{figure}
{{\tt put}} walks the table similarly to {{\tt find}} but there are large differences.
First, {{\tt put}} will allocate and initialize a node when it comes across a $null$ pointer and try to CAS a pointer to that node expecing the $null$ it saw before.
Failure of this CAS implies that some other {{\tt put} succeeded in its own CAS and that node must also be considered.
If a node is reached that contains the desired key, {{\tt put}} can set the value on that node and return, freeing an unsuccessfully CAS'd node if one was created.
When completing an iteration at a level, a {{\tt put}} considers the row's pointer to its next level.
If the pointer is $null$, then the thread allocates the next level and places the pointers it read from the previous row into the new table using the stored hashes in the nodes.
This new level is essentially a local resize of the row into a table.
This allocated table is then CAS'd into the previously-$null$ pointer expecting $null$.
On failure, the allocated table can be freed immediately because no other thread observed it.
The thread then walks the table and continues this procedure until termination.
\subsection{Linearizability}
In order to be linearizable, the {{\tt get}} and {{\tt put} actions need to appear to occur atomically between invocation and response.
A {{\tt put}} that successfully CAS's a node appears to occur when the CAS succeeds.
A {{\tt put}} that modifies an existing node appears to occur when its write completes.
A {{\tt get}} fails only if during the beginning of its procedure, a {{\tt put}} for that key had not yet succeeded.
The linearization point for a {{\tt get}} that fails is its invocation.
A {{\tt get}} that succeeds observes a value in the node it finds.
The {{\tt get}} must have happened after that value was set and after that node was CAS'd,
and so when {{\tt get}}s occur concurrently with {{\tt put}}s on the same key, {{\tt get}}s can only observe values that were {{\tt put}} before or during their operation.
The linearization point for a {{\tt get}} that succeeds is when it reads the value.
\begin{theorem}
    The dictionary presented is linearizable.
\end{theorem}
\subsection{Lock-freedom}
The system is lock-free because when a CAS fails, either in level creation or in node placement, it is because another CAS has succeeded.
Therefore, there is no interleaving that prevents system-wide progress.
\begin{theorem}
    The dictionary presented is lock-free.
\end{theorem}
\subsection{Wait-freedom}
For key-spaces with no hash collisions or a bounded number of collisions per hash, the total work in every operation is bounded by the depth of the structure times the worst-case
iteration behavior, plus the iteration over the total hash collisions in the final level.
The structure cannot guarantee wait-freedom in the presence of an unbounded number of complete hash collisions, because concurrent {{\tt put}}s on keys with the same hash value could repeatedly stall other {{\tt put}}s and {{\tt get}}s for that hash value.
\par
A modification of the structure to support variable-length hashes would be simple.
At every level, an array of $2^b-1$ node pointers would be sufficient to account for the termination of a hash.
A {{\tt find}} or {{\tt put}} for a variable-length hash would behave normally until it expends all but its last $b_h < b$ bits and then it would index into the final array of its current level using those $b_h$ bits.
This structure would be wait-free for any key-space with a bijection to the variable-length bitstrings, because total work would be bounded by the length of the hash.
Therefore, for most practical key types, the proposed dictionary can be made wait-free at the cost of additional time and space at each level.
\subsection{Benchmarking}
The system used for benchmarking was a TACC Stampede Compute Node.
A Compute Node has 2 processor sockets each with an Intel Xeon E5-2680 processor \cite{tacc}.
Each processor clocks at 2.7 GHz and has 20 MB of cache, 8 cores, and 16 threads of execution.
The Compute Node also has 32 GB of memory clocked at 1600 MT/s and a coprocessor which wasn't used.
\par
In the benchmark, the total work to do was $17$ {{\tt put}}s and $16$ {{\tt get}}s for each of 40 million keys.
Each thread performs a {{\tt put}} for each key in its share of the key-space and then cycles over the keys,
performing its share of the remaining $16$ {{\tt put}}s and {{\tt get}}s.
40 million was chosen because for larger work loads memory allocations would fail.
This work was divided among a number of threads but acted on a shared dictionary.
The total time is depicted.
Ideally, as the number of threads increases, the total time decreases as if the work is divided without overhead.
\par
All dictionaries were implemented in C++ and compiled with the same flags,
except for {{\tt-fexceptions}}, which was required for the libcds implementation of the Michael result.
The other dictionaries were compiled with {{\tt-fnoexceptions}}.
\par
For each structure and each number of cores, five trials were measured.
The dictionary introduced performed poorly
in a single-core situation because of its frequent use of strong CAS, but outpaced others as additional cores were added.
The best single-core performer was the STL container $unordered\_map$ (LockSTL).
For more than one thread, LockSTL used a naive mutex for each operation.
All other structures outperformed the LockSTL for more than one thread.
LockSTL's worst performance occurred at 4 cores for an unknown reason.
That spike reproduced on the same machine but did not reproduce on an AMD FX 8350 and so it is probably architecture-specific.
The 2002 Michael dictionary implementation came from Maxim Khizhinsky's libcds.
It had an advantage over other structures in knowing at initialization the number of keys.
It performed well up to 12 cores where it had especially good performance but had unexpectedly poor performance at 16.
The dictionary implemented by Feldman, LaBorde, and Dechev was supplied by the University of Central Florida.
The dictionary was benchmarked with its ThreadWatch enabled, because otherwise it exhibitied instability.
Its performance was best between 4 and 8 threads but adding more increased the total time spent.
A weakness in its performance and a possible reason for its poor performance at higher thread counts is that it allocates a node for every {{\tt put}}.
\par
The dictionary introduced in this paper had the best performance at 16 threads.
Its performance for 16 threads was over $3$ times faster on average than the dictionary of Feldman, LaBorde, and Dechev
and over $50\%$ faster than Michael's lock-free hash table.
Every thread added decreased the total time spent.
This is a desirable trait in a concurrent data structure because the operations are mostly independent and so the structure scales.
However, the single-threaded STL container still beat the sixteen-threaded dictionary.
This can be attributed to the weak performance of cache-coherence in modern hardware.
\par
\begin{figure}[h!]
    \begin{tikzpicture}
        \begin{axis}[
            legend pos=north east,
            xlabel=Locking Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    stl={mark=o, draw=black}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{papers/stl.dat};
            \legend{LockSTL}
        \end{axis}
    \end{tikzpicture}
    \begin{tikzpicture}
        \begin{axis}[
            legend pos=north east,
            xlabel=Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    mich={mark=o, draw=green}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{papers/mich.dat};
            \addplot[
                scatter/classes={
                    feld={mark=square*, blue}   
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{papers/feld.dat};
            \addplot[
                scatter/classes={
                    dict={mark=triangle*, red}
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{papers/dict.dat};
            \legend{Michael2002,Feldman2013,Morriss2015}
        \end{axis}
    \end{tikzpicture}
    \centering
    \caption{Dictionary Performance}
\end{figure}
\pagebreak
\section{Hazard pointers and STWGC}
\subsection{Method}
The FIFO queue designed by Kogan and Petrank relies on Stop-The-World garbage collection (STWGC) for memory reclamation and protection from the ABA problem,
where a slow thread observes no change despite changes having actually happened.
While STWGC is an unfortunate reality of several managed runtimes, it is a strategy incompatible with wait-freedom due to its system-wide blocking of all work.
This section applies Michael's hazard pointers \cite{hazard} to the aforementioned FIFO.
This memory management solution was suggested by Kogan and Petrank in \cite{kogan2011}.
\par
The hazard pointer FIFO was implemented in C++.
Several minor modifications were made to the original structure.
First, the next phase was calculated with a fetch and add on a shared counter.
Fetch and add is constant instead of linear in the number of threads.
On the downside, this means that the counter approaches overflow faster, but still at an unconcerning rate.
Second, phase was separated from the operation description, allowing $null$ operation descriptions to indicate no-help-required.
These reductions simplified memory management.
\par
Hazard pointers need to be declared for every dynamic element.
Both nodes and operation descriptions are dynamically allocated.
A thread working on an operation must iteratively derive and announce the operation description.
If this happened an unbounded number of times then this approach would be lock-free but not wait-free.
However, there is an upper bound on the number of times a thread's operation description can be updated without the thread having completed at least one operation.
For an {{\tt enq}}, this number is $0$ because if the operation description has changed then the operation has completed.
For a $deq$, the calculation is more complicated.
This is because of a precommit operation where before a thread updates the head's decrementing thread ID it must ensure that the $deq$ operation description points to the head.
This update can occur without the completion of the deq because another thread has observed another head.
For $N$ threads, the head can be updated in this way at most $N-1$ times, when each of the other $N-1$ threads are performing a $deq$.
After that number of failures, every thread operating on the queue will be attempting to perform the same $deq$.
On the $N$th failure, the thread that was being helped has completed some operation and so a helper with that many failures can continue.
This bounds the number of retries in each iteration of {{\tt help}}.
\par
Operation descriptions are retired differently in different situations.
An operation description replaced in a precommit update or in committing an $Empty$ result during $deq$ is immediately retired by the replacing thread.
An operation description requesting an {{\tt enq}} is never retired.
Instead, it is linked in the node it requested and freed when that node is freed.
An operation description that commits a succeeding $deq$ is linked to the node that contains the data dequeued.
This link is performed after that data is read.
Nodes are retired by the thread that successfully updates the head.
Retired nodes are freed along with their two associated operation descriptions when both are linked and when no thread is declaring to use them hazardously.
The requirement of linking is needed because the thread attempting to return data may not have been the thread completing its $deq$.
\par
An operation description that requests an {{\tt enq}} could instead be retired by the thread that successfully replaces it with $null$ in {{\tt help\_finish\_enq}}.
This was not tested.
\subsection{Benchmarking}
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            legend pos=south east,
            xlabel=Threads,
            ylabel=Time (s),
            xmin=0,
            ymin=0
        ]
            \addplot[
                scatter/classes={
                    fifo={mark=square, blue}   
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{papers/fifo.dat};
            \addplot[
                scatter/classes={
                    kogan={mark=o, black}   
                },
                scatter,
                only marks,
                scatter src=explicit symbolic
            ]
            table[
                x=threads,
                y=time,
                meta=source
            ]{papers/kogan.dat};
            \legend{Hazard Pointers,Java GC}
        \end{axis}
    \end{tikzpicture}
    \caption{FIFO Performance}
    \label{fifograph}
\end{figure}
Hazard pointers were compared with garbage collection.
The TACC Stampede Compute Node described earlier was used again for the FIFO benchmarking.
The total work was to enqueue and dequeue $6,400,000$ integers.
The total completion time is depicted in Figure \ref{fifograph}.
Ideally the time would decrease as if divided by the number of threads.
The timing of the Java implementation discounted the significant startup time for the JVM by measuring time within the runtime.
The C++ hazard pointers implementation was timed with the {{\tt time}} Bash builtin.
\par
Due to the use of the Kogan structure, both queues were expected to perform worse with each added thread.
Further, the time was expected to increase due to thrashing from cache coherence.
Instead, both queues had surprisingly constant performance.
The performance of the Java implementation varied significantly more than the C++ implementation.
Both implementations experienced a peak at 2 threads, where thrashing hits hardest.
The hazard pointers approach was significantly faster for a single thread, perhaps due to memory locality.
For additional threads, neither approach was especially better than the other in terms of performance,
though the performance of hazard pointers slightly declined, perhaps due to increased memory retention.
\section{Further Work}
The dictionary can be easily modified to support lock-free removal.
Wait-free removal is elusive but not provably impossible for this structure.
An ideal wait-free removal algorithm would not accumulate overhead with additional threads.
\par
A better concurrent queue would allow more threads to concurrently succeed in performing an {{\tt enq}} or a {{\tt deq}}.
The queue of Kogan and Petrank only allows on thread to be succeeding on each end of the queue at a time.
Relaxing first-in-first-out in a way that cannot indefinitely prevent an enqueued item from being dequeued might increase throughput.
A resulting autobahn queue could provide a practical work queue that scales to thousands of threads.
\section{Conclusion}
This paper presented a fast concurrent dictionary.
The dictionary outperforms alternatives at a high thread count and can be augmented to trade space and performance for wait-freedom.
This paper also describes an application of hazard pointers to the queue of Kogan and Petrank that preserves wait-freedom.
Queues and dictionaries are fundamental building blocks of common algorithms such as search.
Increasing their parallel portion increases the possible speedup obtainable as hardware moves towards concurrency.
\pagebreak
\begin{thebibliography}{2}
    \bibitem{amdahl}
    Amdahl, G. M. ``Validity of the single processor approach to achieving large scale computing capabilities.'' In \textit{Proceedings of the AFIPS 1967 Spring Joint Computing Conference}, Vol. 30, pp. 483-485.
    \bibitem{dally}
    Dally, B. (2010). ``Life After Moore's Law.'' In \textit{Forbes}.
    \bibitem{feldman}
    Feldman, LaBorde, and Dechev. ``Concurrent Multi-level Arrays: Wait-free Extensible Hash Maps.'' In \textit{Embedded Computer Systems: Architectures, Modeling, and Simulation (SAMOS XIII), 2013 International Conference on}, pp. 155-163. IEEE, 2013.
    \bibitem{jayanti2005}
    Jayanti, Prasad, and Srdjan Petrovic. (2005). ``Logarithmic-Time Single Deleter, Multiple Inserter Wait-Free Queues and Stacks.'' In \textit{FSTTCS 2005: Foundations of Software Technology and Theorectical Computer Science}, pp. 408-419. Springer Berlin Heidelberg, 2005.
    \bibitem{herlihy88}
    Herlihy, Maurice P. ``Impossibility and universality results for wait-free synchronization.'' In \textit{Proceedings of the seventh annual ACM Symposium on Principles of distributed computing}, pp. 276-290. ACM, 1988.
    \bibitem{herlihy91}
    Herlihy, Maurice P. ``Wait-Free Synchronization''. \textit{ACM Transactions on Programming Languages and Systems (TOPLAS)} 13, no. 1 (1991): 124-149.
    \bibitem{linearizability}
    Herlihy, Maurice P., and Wing, J. ``Linearizability: A Correctness Condition for Concurrent Objects.'' \textit{ACM Transactions on Programming Languages and Systems (TOPLAS)} 12, no. 3 (1990): 463-492.
    \bibitem{ibm}
    IBM. ``IBM System/370 Extended Architecture: Principles of Operation.'' 1987.
    \bibitem{kogan2011}
    Kogan, Alex. and Erez Petrank. ``Wait-Free Queues With Multiple Enqueuers and Dequeuers.'' In \textit{ACM SIGPLAN Notices}, vol. 46, no. 8, pp. 223-234. AMD, 2011.
    \bibitem{michael2002}
    Michael, Maged M. ``High Performance Dynamic Lock-Free Hash Tables and List-Based Sets.'' In \textit{Proceedings of the fourteenth annual ACM symposium on Parallel algorithms and architectures}, pp. 73-82. ACM, 2002.
    \bibitem{hazard}
    Michael, Maged M. ``Hazard Pointers: Safe Memory Reclamation for Lock-Free Objects.'' \textit{Parallel and Distributed Systems, IEEE Transactions on} 15, no. 6 (2004): 491-504.
    \bibitem{tacc}
    ``TACC Stampede User Guide.''
\end{thebibliography}
\end{document}
