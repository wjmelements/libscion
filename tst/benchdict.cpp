#include "dict.h"
#include <pthread.h>

#ifndef N_THREADS
#define N_THREADS 8
#endif

#ifndef N_ITEMS
#define N_ITEMS (800000 / N_THREADS)
#endif

typedef dict<long,long> sdict;

struct worker_arg {
    sdict* d;
    int tid;
    worker_arg
    (   sdict* _d
    ,   int _tid
    ):  d(_d)
    ,   tid(_tid)
    {}
};
void* worker(void* va) {
    struct worker_arg* wa = (struct worker_arg*) va;
    sdict* map = wa->d;
    int tid = wa->tid;
    delete wa;

    for (long i = 0; i < N_ITEMS; i++) {
        long key = i + tid * N_ITEMS;
        map->put(key, 0);
    }
    for (long j = tid; j < tid + 16; j++) {
        long k = j % N_THREADS;
        for (long i = 0; i < N_ITEMS; i++) {
            long key = i + k * N_ITEMS;
            long got = map->get(key);
            map->put(key, got + 1);
        }
    }

    return NULL;
}

int main() {
    sdict d;
    pthread_t threads[N_THREADS];
    for (int i = 0; i < N_THREADS; i++) {
        pthread_create(&threads[i], NULL,  worker, new worker_arg(&d, i));
    }
    for (int i = 0; i < N_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    return 0;
}
